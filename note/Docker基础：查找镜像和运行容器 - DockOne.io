<http://dockone.io/>
//

输入关键字进行搜索

搜索:
发起问题 <http://dockone.io/publish>

  * // 发现
  * // 话题 <http://dockone.io/topic/>
  * // 帮助 <http://dockone.io/help/>

登录 <http://dockone.io/login/> 注册 <http://dockone.io/account/register/>	

<http://dockone.io/article/2626>
Docker <http://dockone.io/topic/Docker>


  Docker基础：查找镜像和运行容器


【编者的话】本文是作者学习Docker的笔记，涉及在Windows上的安装，介绍了镜
像和容器的知识和基本操作，适合Docker初学者。

*【Kubernetes培训通知】DockOne将会于2018年8月24日在北京举办Kubernetes技
术培训 <https://mp.weixin.qq.com/s/Ufq1iwWKzQnf_uKLdNjNjw>，培训内容包
括：容器特性、镜像、网络；Kubernetes架构、核心组件、基本功能；Kubernetes
设计理念、架构设计、基本功能、常用对象、设计原则；Kubernetes的数据库、运
行时、网络、插件已经落地经验；微服务架构、组件、监控方案等*。


      介绍

也许你已经听说过Docker，这里我不打算深入介绍Docker是什么以及它的工作原
理。如果你从未听说过Docker，这篇文章 <https://www.docker.com/what-
docker>会帮助你熟悉Docker。如果你了解Docker的话我们从一些基本的功能说
起：*镜像*和*容器*以及如何使用它们解决真实场景中的问题，比如想要学习一门
很棒的语言又不希望花太多时间在安装语言和环境搭建上。

继续之前，需要说明三点：

  * 这个系列是关于基本操作的，目的在于介绍概念，为深入学习做铺垫。
  * 我并不是这个领域的专家，甚至也不高级。实际上我是初学者并且也没有在任
    何重要项目中使用过Docker。我想看到后一句话很多人已经不打算看了。
  * 我正在使用Windows系统，所以我的操作是在Windows上进行的。这并不意味着
    你不能在Mac或Linux上参考本文，实际上只有安装部分不同，其余的应该是一
    样的。



      安装

参考官方安装指导 <https://docs.docker.com/engine/installation/>，如果是
Windows <https://docs.docker.com/docker-for-windows/install/>，会有两个
选项：直接安装Docker或者Docker Toolbox（Docker工具箱）。需要注意新版本
Docker需要64-bit Windows 10 Pro并且支持Microsoft Hyper-V。如果你的
Windows版本比较老，安装Toolbox，它兼容Oracle Virtual Box。

当使用新版本Docker时，你会得到提示：
docker_tray.png
<http://dockone.io/uploads/article/20170702/64ab9c66bb7d26a3f2e5263c9d779ead.png>


如果你使用Docker Toolbox，会有很多工具，其一是*Docker Quickstart Terminal*。

如果安装原生的Docker，你可以在最爱的终端中运行Docker命令，如果是
Toolbox，在Quickstart Terminal中输入命令。

另一件需要注意的点在于，原生安装Docker时通过localhost访问容器中的
webapps，如果是Toolbox，你需要注意启动Quickstart Terminal时的输出信息，
它会给你容器的IP，在我的电脑上是192.168.99.100。

快速总结下，在Windows上你有两个选择：

  * 原生Docker，你需要64-bit Windows 10 Pro并且支持Microsoft Hyper-V，你
    会有Docker的系统图标，能在任何终端中输入Docker命令
  * Docker Toolbox，你的Windows版本低时的选择。需要安装Oracle Virtual
    Box，启动Quickstart Terminal后输入Docker命令。



      使用

假设一个简单但是贴近现实的例子：学习基于JVM的语言Scala。在成为专家前，你
总是需要学习很多新的知识，你要参考很多基本的入门指南，需要一个REPL执行操
作，但是你不希望在自己的电脑上下载安装Scala。那么如何使用Docker解决这个
问题？很简单，你只需要运行一个包含Scala和相关依赖的容器即可。

如果你是开发者，可以很容易理解镜像和容器的区别，容器相对于镜像就像对象相
对于类。

镜像是从网络上下载的不可变文件，它描述了如何构建特定的容器。它们可能很
大，所以要分模块构建以便在网络上传输。容器是镜像的实例，Docker启动容器后
执行镜像中指定的指令。

在我们的例子中，我们需要搜索Scala镜像并启动容器。


        搜索镜像

两个选项，直接在Google搜索*Scala docker image*或者运行命令*docker search
scala*。
docker_search.png
<http://dockone.io/uploads/article/20170702/ba0ab1f5b2871f7d90ab4438c6f69299.png>


我们以上图中第一个镜像为例，可以登录docker hub（https://hub.docker.com/r
/hseeberger/scala-sbt）了解它的详细信息。


        如何启动容器

启动容器的命令是：

docker run -it hseeberger/scala-sbt


运行|docker run --help|可以查看更详细的信息：

  * -i表示交互式，STDIN会被开启，即使我们没有附着到容器上
  * -t表示tty，我们会得到一个伪tty <https://en.wikipedia.org/wiki/TTY>与
    容器交互



-it经常结合在一起使用。run命令运行结束后你会看到如下内容：

root@3d5b83c7ea03:~#


做了这么多麻烦事就得到一个命令行提示符？显然不，这里你得到的新容器的
shell，时刻准备接收命令。如果执行ls，会看到一个scala-2.12.2目录，我们进
入这个目录然后执行scala，然后得到scala REPL，在这里就可以实战学习scala
了。|Ctrl+C|退出REPL，exit退出容器。


        如何做得更好

我们可以使用第二个镜像 <https://hub.docker.com/r/williamyeh/scala/>，然
后直接运行|docker run -it williamyeh/scala|命令就可以得到scala REPL了。
为何运行两个镜像得到不同的结果？我会在下一篇文章中介绍。

总结一下，运行容器只需要简单运行命令|docker run -it your/image|，运行的
容器会做什么取决于你的镜像了。


      有用的命令

  * docker ps：列出所有正在运行的容器
  * docker image ls：列出所有的镜像
  * docker stop <name>：暂停容器
  * docker rm <name>：删除容器



*原文链接：Docker basics - finding an image and running a container
<https://rskupnik.github.io/docker-series-1-image-and-container>（翻译：
卢文泉）*

// *0* <javascript:;>
// 分享

  * // 微博
  * // QZONE
  * // 微信

/2017-07-01/


    0 个评论

要回复文章请先登录 <http://dockone.io/login/>或注册
<http://dockone.io/account/register/>


      发起人

adolphlwq <http://dockone.io/people/adolphlwq>
    adolphlwq <http://dockone.io/people/adolphlwq>

    https://blog.adolphlwq.xyz

擅长话题 :   CentOS <http://dockone.io/topic/CentOS>


      推荐内容

  * 如何Docker化端到端验收测试 <http://dockone.io/article/8121>
  * 如何在 Kubernetes 之上架构应用？ <http://dockone.io/article/8118>
  * 有赞是如何高效管理自己的开发测试环境的？
    <http://dockone.io/article/8112>
  * DockOne微信分享（一八一）：小米弹性调度平台Ocean
    <http://dockone.io/article/7735>
  * 从零开始Docker化你的Node.js应用 <http://dockone.io/article/6096>
  * 可能是把Docker的概念讲的最清楚的一篇文章
    <http://dockone.io/article/6051>
  * Docker、Kubernetes、Containers解读 <http://dockone.io/article/6005>
  * Docker公司新发布的多云战略，能让他在B端扳回一局吗？
    <http://dockone.io/article/6000>
  * Docker Hub发现带有挖矿后门的镜像，已被下载500万次
    <http://dockone.io/article/5996>
  * Docker努力实现跨云联合容器管理 <http://dockone.io/article/5989>


      相关问题

  * Docker如何为企业产生价值？ <http://dockone.io/question/151>
  * 大家对国内的Docker meetup活动组织有什么建议和想法？比如希望在哪些城
    市举办？希望听到什么样的话题？ <http://dockone.io/question/341>
  * 请教下代码放在Docker里面还是外面呢 <http://dockone.io/question/24>
  * 如何学习Docker <http://dockone.io/question/10>
  * Docker container 到底应该翻译成什么？容器？集装箱？
    <http://dockone.io/question/408>
  * Machine、Swarm、Compose、SocketPlane这些Docker生态圈软件各解决了哪些
    问题？ <http://dockone.io/question/160>
  * 国内有哪些Docker大牛？ <http://dockone.io/question/430>
  * 容器里Nginx、MySQL的配置文件、日志是否应该挂载到宿主上？
    <http://dockone.io/question/54>
  * docker书籍 <http://dockone.io/question/574>
  * Container内不需要OS，为何需要OS的基础镜像？
    <http://dockone.io/question/6>
  * Docker容器使用--net=host的方式启动的，怎么用ssh去连接容器
    <http://dockone.io/question/199>


      DockOne.io，专业的容器技术交流平台

关注容器相关的产品以及开源项目

2018 *DockOne*. All Rights Reserved.

DockOne，新圈子，新思路，新视野。

本网站服务器由UCloud云服务
<http://www.ucloud.cn/?utm_source=zanzhu&utm_campaign=DokerOne&
utm_medium=display>提供。

  * Weibo <http://weibo.com/dockerone>
  * Wechat
  * 周报 <http://weekly.dockone.io/>
  * Feed <http://dockone.io/feed>

/

/ <javascript:;>
