from operator import itemgetter
import codecs
import urllib
import urllib.request  
import re
#import pandas as pd
#import pymysql
import os
import gc
import time
import minpb2_lib
#from predict2 import predict_gupiao_k_0day_lib
from predict2 import predict_gupiao_k_0day_lib_v0
from sys import argv
import datetime
import math
print(datetime.datetime.now())
the_now_hour=int(datetime.datetime.now().strftime("%H"))
print(the_now_hour)
the_now_minute=int(datetime.datetime.now().strftime("%M"))
print(the_now_minute)

print("--this is a95--")
#the_now_hour=11# for temp test
#the_now_minute=45#for temp test
if (the_now_hour==12 and the_now_minute>13) or (the_now_hour==15 and (the_now_minute<13 or the_now_minute>28)):
	print("--this is no need run a95 at 12:15-12:59 or at 15:00 or after 15:30")
	#quit()


print(argv)
print(len(argv))
ask_risk_level=""
ask_kind=""
if len(argv)==0:
	#ask_risk_level="low_risk"
	#ask_kind="3jlt"
	ask_risk_level="all"
	ask_kind="all"
	
	print("notice:use the default args is: all all !")
elif len(argv)==3:
	ask_risk_level=argv[1]
	ask_kind=argv[2]
elif len(argv)==2:
	ask_risk_level=argv[1]
	ask_kind="all"
else:
	print("please use this script as this: show_risk_level [low,middle,all] [3jlt,all]")

print(ask_risk_level,ask_kind)


low_to_middle_risk_threshold=1.1
middle_to_high_risk_threshold=1.5
ask_all_risk_level_flag=0
if ask_risk_level=="low":
	ask_risk_threshold=low_to_middle_risk_threshold
elif  ask_risk_level=="middle":
	ask_risk_threshold=middle_to_high_risk_threshold
else:
	
	ask_all_risk_level_flag=1


ask_all_kind_flag=0

if ask_kind=="3jlt":
	ask_kind="3jlt"
else:
	ask_all_kind_flag=1
	ask_kind="all"

#input data
input_file=codecs.open("a30.csv", 'r','utf-8')

input_file2=codecs.open("3jlt.csv", 'r','gb2312')


#print("--debug1")

#output data
output_file=codecs.open("top100_%s_%s.csv"%(ask_risk_level,ask_kind),'w+','utf-8')

#print("--debug2")

table_3jlt=[]

temp_one_row=[]
gupiaocode_col_n=0
name3jlt_col_n=4


for x in input_file2:
	str_temp_one_row=x.strip()
	temp_one_row=str_temp_one_row.split(",")
	str_gupiaocode=temp_one_row[gupiaocode_col_n]
	str_gupiaocode=str_gupiaocode[0:6]
	temp_one_row[gupiaocode_col_n]=str_gupiaocode
	str_name3jlt=temp_one_row[name3jlt_col_n]
	table_3jlt.append(temp_one_row)

input_file2.close()






input_file2b=codecs.open("3jlt_add_gnlt.csv", 'r','gb2312')

temp_one_row=[]
gupiaocode_col_n=0
name3jlt_col_n=4
my_gnlt=[]
for x in input_file2b:
	str_temp_one_row=x.strip()
	temp_one_row=str_temp_one_row.split(",")
	str_gupiaocode=temp_one_row[gupiaocode_col_n]
	str_gupiaocode=str_gupiaocode[0:6]
	temp_one_row[gupiaocode_col_n]=str_gupiaocode
	str_name3jlt=temp_one_row[name3jlt_col_n]
	table_3jlt.append(temp_one_row)
	my_gnlt.append(str_name3jlt)

input_file2b.close()





input_file2c=codecs.open("gupiao_3jlt_dcw.csv", 'r','utf-8')
temp_one_row=[]
gupiaocode_col_dcw_n=0
gupiaoname_col_dcw_n=1
name3jlt_col_dcw_n=2
my_gnlt_dcw=[]
for x in input_file2c:
	str_temp_one_row=x.strip()
	temp_one_row=str_temp_one_row.split(",")
	str_gupiaocode=temp_one_row[gupiaocode_col_dcw_n]
	str_gupiaoname=temp_one_row[gupiaoname_col_dcw_n]
	str_gupiaocode=str_gupiaocode[0:6]
	temp_one_row[gupiaocode_col_n]=str_gupiaocode
	str_name3jlt=temp_one_row[name3jlt_col_dcw_n]+'_'+temp_one_row[name3jlt_col_dcw_n+1]+'_'+temp_one_row[name3jlt_col_dcw_n+2]
	my_gnlt_dcw.append([str_gupiaocode,str_gupiaoname,str_name3jlt])

input_file2c.close()













#print(table_3jlt[199][gupiaocode_col_n],table_3jlt[199][name3jlt_col_n])
#exit()

table = []
str_header = input_file.readline()

str_header = str_header.strip()

top_pai_ming_col_no=0;
code_col_no=1;
name_col_no=2;
trade_no=5;
roa_col_no=9
turnoverratio_col_no=10
per_col_no=12
pb_col_no=13;
minpb2_col_no=18;
days_after_ipo_col_no=19;

col_name_risk_level="risk_lv"#risk_level
col_name_ratio_of_pb_to_minpb2="pb_paomo"#pb_vs_minpb2   pb pao mo = pb / minpb2
col_name_zhi_neng_cang_wei="智能仓位"#"zncw0"
col_name_zhi_neng_ye_ce="智能多空"#"znyc0"
col_name_3jlt_name="the3jlt"#the_3jlt_name
col_name_AICW="AICW"
col_name_AIDK="AIDK"
#result_table=[];

merged_header=[]
str_merged_header=""

merged_line=[]
str_merged_line=""
k=0

#print("--debug3")
print("-----------------reuslt----------------")
#only out the first 100 line
my_k=1
for str_line in input_file:
	#print("--debug4")

	print(str_line)
	col = str_line.split(",")
	#print(col)
	top_pai_ming = int(col[top_pai_ming_col_no])
	gupiao_code = str(col[code_col_no])
	gupiao_name = str(col[name_col_no])
	
	trade=float(col[trade_no])
	
	gupiao_turnoverratio = float(col[turnoverratio_col_no])
	gupiao_turnoverratio = round(gupiao_turnoverratio,5)
	col[turnoverratio_col_no]=str(gupiao_turnoverratio)

	gupiao_roa = float(col[roa_col_no])
	gupiao_roa = round(gupiao_roa,1)
	col[roa_col_no]=str(gupiao_roa)

	gupiao_per = float(col[per_col_no])
	gupiao_per = round(gupiao_per,3)
	col[per_col_no] = str(gupiao_per)

	gupiao_pb = float(col[pb_col_no])
	gupiao_pb = round(gupiao_pb,3)
	col[pb_col_no] = str(gupiao_pb)

	gupiao_minpb2 = float(col[minpb2_col_no])
	risk_level="high risk"
	ratio_of_pb_to_minpb2=99
	if gupiao_pb>0 and gupiao_minpb2 > 0 :
		ratio_of_pb_to_minpb2 = round(gupiao_pb / gupiao_minpb2,2)
	
	if ratio_of_pb_to_minpb2>middle_to_high_risk_threshold:
		risk_level="high_risk"
	elif ratio_of_pb_to_minpb2>low_to_middle_risk_threshold:
		risk_level="middle_risk"
	else:
		risk_level="low_risk"
	
	str_risk_level=str(risk_level)
	str_ratio_of_pb_to_minpb2=str(ratio_of_pb_to_minpb2)
	

	
	if str_merged_header=="":
		#print(str_header)
		
		#str_merged_header="%s,%s,%s"%(str_header,col_name_risk_level,col_name_ratio_of_pb_to_minpb2)
		str_merged_header="%s,%s,%s,%s,%s,%s,%s,%s"%(col_name_risk_level,col_name_ratio_of_pb_to_minpb2,col_name_3jlt_name,col_name_zhi_neng_cang_wei,col_name_zhi_neng_ye_ce,str_header.strip(),col_name_AICW,col_name_AIDK)

		output_file.write(str_merged_header)	
		print(str_merged_header)

		output_file.write("\n")
	

	#str_merged_line="%s,%s,%s"%(str_line.strip(),str_risk_level,str_ratio_of_pb_to_minpb2)
	
	seeked_3jltname="-"
	
	seeked_2jltname=''
	for a_item in my_gnlt_dcw:
		if gupiao_code==a_item[gupiaocode_col_dcw_n]:
			seeked_2jltname=a_item[name3jlt_col_dcw_n]
			
	for row_table_3jlt in table_3jlt:
		if gupiao_code==row_table_3jlt[gupiaocode_col_n]:
			seeked_3jltname=row_table_3jlt[name3jlt_col_n]
	
	seeked_3jltname=seeked_2jltname+seeked_3jltname
	
	
	
	
	if  ask_all_risk_level_flag==1 or ratio_of_pb_to_minpb2<=ask_risk_threshold:
				#print("debug 01",ask_all_kind_flag,seeked_3jltname)
				if  (ask_risk_level=='middle' or ask_risk_level=='low') and ask_all_kind_flag==1 and gupiao_roa<8:
					print('gupiao_roa:',gupiao_roa)
					print('gupiao_per:',gupiao_per)
					print('gupiao_roa<8,this gu piao has NO jia zhi!')
					continue

				if not seeked_3jltname=="-":



					###zhi neng cang wei 0 begin
					days_after_ipo = int(col[days_after_ipo_col_no])
					zhi_neng_cang_wei=0
					#math.log((0.5/1.5),0.92) is 13.17
					#max_cang_wei_per_stock=(1+1+13.17)*(1+13.17)/2.0*1000 #jin zi ta ,xiao hao zi jin da duo!
					#>>> jin_zi_ta_l_level=math.log((0.5/1.5),0.95) # this is NOT jin zi ta,please use it!
					'''
					>>> math.log((1.5/1.5),0.95)
					-0.0
					>>> math.log((1.4/1.5),0.95)
					1.3450661009540625
					>>> math.log((1.3/1.5),0.95)
					2.7898548016718023
					>>> math.log((1.2/1.5),0.95)
					4.350345478460224 
					>>> math.log((1.1/1.5),0.95)
					6.04669542105132
					>>> math.log((1.0/1.5),0.95)
					7.904836547339706
					>>> math.log((0.9/1.5),0.95)
					9.958916265085392
					>>> math.log((0.8/1.5),0.95)
					12.255182025799929
					>>> math.log((0.7/1.5),0.95)
					14.858473434918936
					>>> math.log((0.6/1.5),0.95)
					17.863752812425098 
					>>> math.log((0.5/1.5),0.95)
					21.41824388130458
					>>> math.log((0.4/1.5),0.95)
					25.768589359764803
					>>> math.log((0.3/1.5),0.95)
					31.377160146389972  # yuz say:may be this is max zhi neng cang wei........						
					>>> math.log((0.2/1.5),0.95)
					39.281996693729674
					>>> math.log((0.1/1.5),0.95)
					52.79540402769455				
					>>> math.log((0.01/1.5),0.95)
					97.68597150804939
					'''

					max_cang_wei_per_stock=(32+0)*3000 #not jin zi ta
					print("debug 01: %s"%max_cang_wei_per_stock,ratio_of_pb_to_minpb2)
					if ratio_of_pb_to_minpb2<=0.3:
						jin_zi_ta_l_level=32
					elif ratio_of_pb_to_minpb2>=1.5:
						jin_zi_ta_l_level=0
					else:
						jin_zi_ta_l_level=math.log((ratio_of_pb_to_minpb2/1.5),0.95)
					
					if jin_zi_ta_l_level<=0:
						jin_zi_ta_l_level=0

					#zhi_neng_cang_wei=(1+1+jin_zi_ta_l_level)*(1+jin_zi_ta_l_level)/2.0*1000#jin zi ta ,xiao hao zi jin da duo!
					zhi_neng_cang_wei=(0+jin_zi_ta_l_level)*3000
					print("debug 02: %s"%zhi_neng_cang_wei)
					
					
					if len(seeked_3jltname)>=1\
					and (seeked_3jltname.find('券商')>=0\
					or seeked_3jltname.find('证券')>=0\
					or seeked_3jltname.find('黄金')>=0):
						zhi_neng_cang_wei=zhi_neng_cang_wei*2.0
					else:						
						zhi_neng_cang_wei=zhi_neng_cang_wei*(days_after_ipo*1.0/2500)

					if len(gupiao_code)>=3 and len(seeked_3jltname)>=1:
						if gupiao_code[0:3]=='300':
							zhi_neng_cang_wei=zhi_neng_cang_wei*0.1
						else:
							if seeked_3jltname=="-":
								zhi_neng_cang_wei=zhi_neng_cang_wei*0.5
					
					if trade==0:#股票停牌
						zhi_neng_gu_fen=0
					else:
						zhi_neng_gu_fen=zhi_neng_cang_wei*1.0/trade
					
					#zhi_neng_cang_wei=zhi_neng_cang_wei*1.0/10000
					#zhi_neng_cang_wei=round(zhi_neng_cang_wei,1)
					zhi_neng_cang_wei=math.floor(zhi_neng_cang_wei/1000)
					zhi_neng_cang_wei=zhi_neng_cang_wei/10
					if zhi_neng_cang_wei<=0:
						zhi_neng_cang_wei=0
					str_zhi_neng_cang_wei=str(zhi_neng_cang_wei)+'w'
					
					zhi_neng_gu_fen = int(zhi_neng_gu_fen*1.0/100)*100
					if zhi_neng_gu_fen<=0:
						zhi_neng_gu_fen=0
					str_zhi_neng_gu_fen=str(zhi_neng_gu_fen)+'股'
					str_zhi_neng_cang_wei=str_zhi_neng_cang_wei+'['+str_zhi_neng_gu_fen+']'
					str_AICW=str_zhi_neng_gu_fen

					###zhi neng cang wei 0 end
					
					str_zhi_neng_yu_ce='-'
					temp_str='-'
					#the_now_hour=16#temp add for test
					#if (the_now_hour==15 and the_now_minute>42) or (the_now_hour>15):
					if 1 or (my_k<=101 and (the_now_hour==12 or the_now_hour==16)):
						if 1 or top_pai_ming<=100\
										or (not seeked_3jltname=='-'):

							print(datetime.datetime.now())
							print('it is time for predict K!')
							str_zhi_neng_yu_ce='不明'
							str_gupiao_code=gupiao_code
							#the_predict_output=predict_gupiao_k_0day_lib.predict_month_k_0day(str_gupiao_code)#please bao liu this line!!!
							the_predict_output=predict_gupiao_k_0day_lib_v0.predict_week_k_0day(str_gupiao_code)
							str_zhi_neng_yu_ce=the_predict_output[1]+'[chance:'+'%3.2f'%(the_predict_output[2])+']'
							print(str_zhi_neng_yu_ce)							
							if str_zhi_neng_yu_ce.find('多')>=0:
								temp_str='<font color="red">&#x21E7</font>'
							elif str_zhi_neng_yu_ce.find('空')>=0:
								temp_str='<font color="green">&#x21E9</font>'
							else:
								temp_str='<font color="yellow">&#x21E8</font>'
							str_zhi_neng_yu_ce=str_zhi_neng_yu_ce+temp_str
					
					str_AIDK=temp_str

					#str_merged_line="%s,%s,%s,%s,%s,%s"%(str_risk_level,str_ratio_of_pb_to_minpb2,seeked_3jltname,str_zhi_neng_cang_wei,str_zhi_neng_yu_ce,str_line.strip())
					print (col)
					str_merged_line="%s,%s,%s,%s,%s,%s,%s,%s"%(str_risk_level,str_ratio_of_pb_to_minpb2,seeked_3jltname,str_zhi_neng_cang_wei,str_zhi_neng_yu_ce,','.join(col).strip(),str_AICW,str_AIDK)				
					output_file.write(str_merged_line)
					output_file.write("\n")
					print(str_merged_line)
					
					my_k = my_k + 1
					if my_k > 600:#should be 100,temp modify for test
						break
	
	
	k=k+1
	#if k>1:
	#	break
	
	

	

#print(result_table);

input_file.close()

output_file.close()
#print(my_gnlt_dcw)
