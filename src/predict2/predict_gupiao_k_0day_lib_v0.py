
import os
#from tensorflow.keras.models import Sequential
#from tensorflow.keras.models import load_model
#from tensorflow.keras.layers import LSTM, Dense
#import tensorflow as tf


import numpy as np
import pandas as pd
import codecs
#import matplotlib.pyplot as plt


import tushare as ts
import pandas as pd
import numpy as np
from sys import argv
import datetime
#from numba import jit

def predict_week_k_0day(str_gupiao_code):	
	print(datetime.datetime.now())
	print(datetime.datetime.now().year)
	print(datetime.datetime.now().month)
	print(datetime.datetime.now().day)
	this_year=datetime.datetime.now().year
	last_year=this_year-1
	last_last_year=this_year-2
	datafilepath = os.getcwd() + os.sep +"predkdata"+os.sep

	if not os.path.exists(datafilepath):
		os.mkdir(datafilepath)

	the_train_start_day='%04d-%02d-%02d'%(last_last_year,1,1)
	the_train_end_day='%04d-%02d-%02d'%(datetime.datetime.now().year,datetime.datetime.now().month,datetime.datetime.now().day)
	the_predict_start_day=the_train_end_day

	print(the_train_start_day)
	print(the_train_end_day)
	print(the_predict_start_day)

	str_gupiao_price_w_qfq_filename=datafilepath+'%sw.csv'%(str_gupiao_code)

	str_gupiao_price_w_k_predict_filename=datafilepath+'predict_week_K_%s.csv'%(str_gupiao_code)
	str_train_data_filename=datafilepath+'%sw_train_data.csv'%(str_gupiao_code)
	str_predict_data_filename=datafilepath+'%sw_predict_data.csv'%(str_gupiao_code)

	#get train data
	if os.path.exists(str_gupiao_price_w_qfq_filename):
		print("file %s has exist."%(str_gupiao_price_w_qfq_filename))
		df=pd.read_csv(str_gupiao_price_w_qfq_filename)     #读入股票数据		
	else:
		print("download stock week price of this year and last year, and saving data to file %s ..."%(str_gupiao_price_w_qfq_filename))
		df=ts.get_k_data(str_gupiao_code, ktype='W', autype='qfq',start=the_train_start_day,end=the_train_end_day)
		df.to_csv(str_gupiao_price_w_qfq_filename)


	data=np.array(df[['close']])   #获取close价序列
	data_multi_features=np.array(df[['open','close','high','low']])   


	class_ids_forfileoutput = ["多", "空", "不明"]
	class_ids = ["rise","fall","unknown"]


	print('debug01')
	print(data)
	print(data.shape)
	if data.shape[0]<62:
		print("When the number of week K is less than 62,we can not predict!")
		the_output=[[str_gupiao_code,'不明',1.0]]	
		return the_output[0]

	#if  data_multi_features[-1][3]<data_multi_features[-2][3]:
	#	the_output=[[str_gupiao_code,'空',0.71]]
	#	return the_output[0]
	print('debug02')
	data_fanzhuan=data[::-1]
	print(data_fanzhuan[0:30])	
	the30weekavg_thisweek=np.mean(data_fanzhuan[0:30,0:1],axis=0)
	print('the30weekavg_thisweek:',the30weekavg_thisweek)
	'''
	the30weekavg_lastweek=np.mean(data_fanzhuan[1:31,0:1],axis=0)
	the30weekavg_prev15week=np.mean(data_fanzhuan[15:45,0:1],axis=0)
	the30weekavg_prev30week=np.mean(data_fanzhuan[30:60,0:1],axis=0)
	print(the30weekavg_prev30week,the30weekavg_prev15week,the30weekavg_lastweek,the30weekavg_thisweek)
	print(data[-1])
	'''
	df2=ts.get_k_data(str_gupiao_code, ktype='D', autype='qfq',start='',end='')
	data2=np.array(df2[['close']])
	print('the price of now:',data2[-1])

	if  data2[-1]<=the30weekavg_thisweek :#or the30weekavg_thisweek<=the30weekavg_lastweek or the30weekavg_thisweek<=the30weekavg_prev15week or the30weekavg_thisweek<=the30weekavg_prev30week:
		the_output=[[str_gupiao_code,'空',0.51]]
	else:
		the_output=[[str_gupiao_code,'多',0.51]]
	return the_output[0]
#####################################################################################################################

