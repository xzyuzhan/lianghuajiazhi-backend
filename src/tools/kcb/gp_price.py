#!/usr/bin/python3

from operator import itemgetter
import codecs
import urllib
import urllib.request  
import re
import os
import time
import datetime
import math
from sys import argv



proxy_support = urllib.request.ProxyHandler({'http': '10.167.196.133:8080'})
opener = urllib.request.build_opener(proxy_support)
urllib.request.install_opener(opener)


if len(argv)==1:
	str_gupiao_code="600837"
	print("notice:code is not specified,6xxxxx will be used as default code!")
else:
	str_gupiao_code=argv[1]
the_input_str=''
while the_input_str!='0':
	the_input_str = input("please input code(input 0 will quit)：")
	if the_input_str!='':
		str_gupiao_code=the_input_str
		

	if str_gupiao_code[0]=="6":
		shanghai_shenzhen_flag=0
		dwf_link_shanghai_shenzhen_flag="sh"
	else:
		shanghai_shenzhen_flag=1
		dwf_link_shanghai_shenzhen_flag="sz"

	os_slash=os.sep
	if os.path.exists('/storage/sdcard1'): 
		datafilepath = '/storage/sdcard1/tempdata/'
	elif os.path.exists('/storage/sdcard0'): 
		datafilepath = '/storage/sdcard0/tempdata/'
	else:
		datafilepath = os.getcwd() + os_slash +"tempdata"	+os_slash
	

	if not os.path.exists(datafilepath):
		os.makedirs(datafilepath)




	the_now=datetime.datetime.now()
	print(the_now)
	the_year=int(the_now.strftime("%Y"))

	the_month=int(the_now.strftime("%m"))

	the_day=int(the_now.strftime("%d"))


	this_year=the_year

	look_years_num=this_year-2008


	start_year=this_year-look_years_num
	end_year=this_year-1
	str_start_day="%s0101"%(start_year)
	str_end_day="%s1231"%(end_year)



	output_gupiao_code_list=[str_gupiao_code]

	str_gupiao_sn_filename=datafilepath+'the_gupiao_data'+"_sn"+".html"
	url_sn0='http://hq.sinajs.cn/list='
	url_sn1=''
	output_sn_gupiao_data_list=[]
	output_a00_beiyong_csv_data_str_line_list=[]
	k=0
	len_of_output_gupiao_code_list=len(output_gupiao_code_list)
	for a_gupiao_code in output_gupiao_code_list:
		str_gupiao_code=a_gupiao_code
		if str_gupiao_code[0]=='6':
			the_shanghai_shenzhen_flag='sh'
		else:
			the_shanghai_shenzhen_flag='sz'
		url_sn1=url_sn1+the_shanghai_shenzhen_flag+str_gupiao_code+','
		k=k+1
		if k%500==0 or k==len_of_output_gupiao_code_list:		

			urllib.request.urlretrieve(url_sn0+url_sn1, str_gupiao_sn_filename)
			input_sn_file1=codecs.open(str_gupiao_sn_filename, 'r','GB18030')
			for line in input_sn_file1:
				output_sn_gupiao_data_list.append(line)
			url_sn1=''

	trade=0

	k=0
	the_output2_lines=[]

	the_price='-'
	the_change='-'
	for str_line in output_sn_gupiao_data_list:
		k=k+1

		str_gupiao_code=str_line[13:19]

		sub_str1=str_line[21:-3]

		cols_p1 =[str(k),str_gupiao_code]
		cols_p2 = sub_str1.split(',')

		cols_all=cols_p1+cols_p2

		the_output2_lines.append(cols_all)

	trade=float(the_output2_lines[0][5])
	the_amount=float(the_output2_lines[0][11])
	the_amount_outputstr='%8.2f yi'%(round(the_amount*1.0/100000000,2))
	#the_price=trade
	the_change=(float(the_output2_lines[0][5])-float(the_output2_lines[0][4]))/float(the_output2_lines[0][4])*100

	print('[result]')
	print('%10.3f(%10.3f%%)  %s'%(trade,round(the_change,2),the_amount_outputstr))
	input_sn_file1.close()
	os.remove(str_gupiao_sn_filename)