﻿from operator import itemgetter
import codecs
import urllib
import urllib.request  
import re
import os
import time
import datetime
import math
from sys import argv

from selenium import webdriver
import time
from selenium.webdriver.support.select import Select
from selenium.webdriver.chrome.options import Options

div=10
allM=1200000/div
chromeOptions = webdriver.ChromeOptions()
chromeOptions.add_argument('--headless')
chromeOptions.add_argument('--ignore-certificate-errors')
chromeOptions.add_argument("--proxy-server=http://10.167.196.133:8080")

print("#####H gu diag tool 0.2.0#####")
if len(argv)==1:
	str_gupiao_code='06837'
	print("notice:gu piao code is not specified,06837 will be used as default [gu piao code]!")
else:
	str_gupiao_code=argv[1]
print("An example of a call to this script:")
print("minpb [gu piao code]   #[gu piao code] is an integer such as 06837")
the_input_str=''
while the_input_str!='0':
	the_input_str = input("please input code：")
	if the_input_str=='0':
		break
	if the_input_str!='':
		str_gupiao_code=the_input_str


	driver = webdriver.Chrome(chrome_options = chromeOptions)

	driver.implicitly_wait(0.001)

	driver.get("http://quote.eastmoney.com/hk/{0}.html".format(str_gupiao_code))
	print('debug 1')

	xpath = "//span[@class='{0}']".format('quote_title_0')
	result = driver.find_element_by_xpath(xpath)
	text0A = result.text
	xpath = "//span[@class='{0}']".format('quote_title_1')
	result = driver.find_element_by_xpath(xpath)
	text0B = result.text
	#print(text0B,text0A)
	the_output_result1=[]
	the_output_result2={}
	for k in range(1,40):

		xpath1 = "//li[@data-cc='{0}']".format(k)
		xpath_p = "//p[@data-cc='{0}']".format(k)
		xpath_td = "//p[@data-cc='{0}']".format(k)
		xpath_all = "//*[@data-cc='{0}']".format(k)	

		try:
			result1 = driver.find_element_by_xpath(xpath_all)
			text1 = result1.text
			print('//* data-cc="{0}" :{1}'.format(k,text1))
			#print(text1_ary)
			if k==5:#/td data-cc="5" :8.220  #need special #gu piao price of now 
				text1_a=['价格',text1]
			elif k==6:#<i data-cc="6" class="red" style="">+0.030</i>  #need special #gu piao price change(p) of today 
				text1_a=['价格变化元',text1]		
			elif k==7:#<i data-cc="7" class="red" style="">+0.37%</i>  #need special #gu piao price change(percent) of today 
				text1_a=['价格变化百分比',text1]		
			else:# the other
				text1_ary=text1.split(':')
				text1_a=[text1_ary[0],text1_ary[1]]
			the_output_result1.append(text1_a)
				
			#the_output_result1.append(text1_ary)
		except Exception as e:
			pass#print(e)

	print(the_output_result1)
	for item in  the_output_result1:
		if item[0]=='价格':
			the_output_result2['trade']=item[1]	#dividend yield
		if item[0]=='价格变化元':
			the_output_result2['changeofprice']=item[1]	
		if item[0]=='价格变化百分比':
			the_output_result2['change']=item[1]	
		if item[0]=='市净率':
			the_output_result2['pb']=item[1]
		if item[0]=='成交额':
			if item[1][-1]=='亿':
				the_output_result2['amount']=float(item[1][:-1])
			else:#'万'
				the_output_result2['amount']=float(item[1][:-1])/10000				
		if item[0]=='市净率':
			the_output_result2['pb']=item[1]
		if item[0]=='每股净资产':
			the_output_result2['b']=item[1]		
		if item[0]=='市盈率':
			the_output_result2['pe']=item[1]		
		if item[0]=='股息率':
			the_output_result2['dy']=item[1]	#dividend yield

	print(the_output_result2)
	driver.close()


	the_amount=the_output_result2['amount']
	the_minpb=0.5
	the_cur_pb=float(the_output_result2['pb'])
	the_cur_pe=float(the_output_result2['pe'])
	the_dy=the_output_result2['dy']
	trade=float(the_output_result2['trade'])
	the_change=the_output_result2['change']


	'''
	#this is test data
	the_amount=0.75562
	the_minpb=0.5
	the_cur_pb=0.6
	the_cur_pe=10.960
	trade=8.220
	the_change=0.37
	the_dy=2.7
	'''
	print('the_amount={0} yi'.format(the_amount))
	print('the_minpb={0}'.format(the_minpb))
	print('the_cur_pb={0}'.format(the_cur_pb))
	print('the_cur_pe={0}'.format(the_cur_pe))
	print('the_dy={0}'.format(the_dy))
	print('trade={0}'.format(trade))
	print('the_change={0}'.format(the_change))





	print(text0B,text0A)
	if the_cur_pb<=0 or the_cur_pe<=0:
		print("[current pb or current pe <=0 ! ] mean this gu piao kuai po can le! do NOT buy this gu piao]")
		continue

	if the_amount<0.001:
		print("[amount<0.001 yi ! ] mean this gu piao is too cold , do NOT buy this gu piao]")
		continue
	else:
		print("[the min pb]")
		print('%10.2f'%(the_minpb))
		print("[current pb]")
		#print(cur_pb)
		the_cur_pb=float(the_cur_pb)
		print('%10.2f'%(the_cur_pb))
		print("[pb pao mo]")
		the_pb_paomo=round(the_cur_pb/the_minpb,2)
		print('%10.3f'%(the_pb_paomo))

		print("[risk level]")
		if the_pb_paomo<=1.1:
			print('     low risk')
		elif the_pb_paomo<=1.5:
			print('     middle risk')
		else:
			print('     high risk')

		print('[dividend yield]')
		print('     {}'.format(the_dy))
		
		print('[zhi neng cang wei]')

		ratio_of_pb_to_minpb2=the_pb_paomo
		#a_buy_of_per_wang_ge=3000*2
		a_buy_of_per_wang_ge=allM/20
		zhi_neng_cang_wei=0
		max_cang_wei_per_stock=(32+0)*a_buy_of_per_wang_ge #not jin zi ta
		if ratio_of_pb_to_minpb2<=0.3:
			jin_zi_ta_l_level=32
		elif ratio_of_pb_to_minpb2>=1.5:
			jin_zi_ta_l_level=0
		else:
			jin_zi_ta_l_level=math.log((ratio_of_pb_to_minpb2/1.5),0.95)

		if jin_zi_ta_l_level<=0:
			jin_zi_ta_l_level=0

		zhi_neng_cang_wei=(0+jin_zi_ta_l_level)*a_buy_of_per_wang_ge
		gupiao_code=str_gupiao_code


		output_gupiao_code_list=[str_gupiao_code]



		the_amount_outputstr='%8.2f yi'%(round(the_amount*1.0,2))

		if trade==0:
			zhi_neng_gu_fen=0
		else:
			zhi_neng_gu_fen=zhi_neng_cang_wei*1.0/trade


		zhi_neng_cang_wei=math.floor(zhi_neng_cang_wei/1000)
		zhi_neng_cang_wei=zhi_neng_cang_wei/10
		if zhi_neng_cang_wei<=0:
			zhi_neng_cang_wei=0
		str_zhi_neng_cang_wei='%3.1fw'%(round(zhi_neng_cang_wei,1))


		zhi_neng_gu_fen = int(zhi_neng_gu_fen*1.0/100)*100
		if zhi_neng_gu_fen<=0:
			zhi_neng_gu_fen=0
		str_zhi_neng_gu_fen=str(zhi_neng_gu_fen)+'gu'
		str_zhi_neng_cang_wei=str_zhi_neng_cang_wei+'['+str_zhi_neng_gu_fen+']'
		print('     %s'%(str_zhi_neng_cang_wei))
		print('[stock price]')
		print('%10.3f(%s)  %s'%(trade,the_change,the_amount_outputstr))

driver.quit()