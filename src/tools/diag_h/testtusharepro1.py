#!/usr/bin/python3
'''
pip3 install tushare --upgrade --proxy 10.167.196.133:8080
'''
import os
import tushare as ts

print(ts.__version__)


def use_proxy():
	HTTP_PROXY = "http_proxy"
	HTTPS_PROXY = "https_proxy"
	if (HTTP_PROXY in os.environ):
		print(os.environ[HTTP_PROXY])
	else:
		print("not set ", HTTP_PROXY)
		os.environ[HTTP_PROXY] = "10.167.196.133:8080"
	if (HTTPS_PROXY in os.environ):
		print(os.environ[HTTPS_PROXY])
	else:
		print("not set ", HTTPS_PROXY)
		os.environ[HTTPS_PROXY] = "10.167.196.133:8080"
	print(os.environ["http_proxy"])
	print(os.environ["https_proxy"])


use_proxy()


your_token='329b6a50f51130370130a038aa56dff20199043d52d684ea6101707b'
pro = ts.pro_api(your_token)


ts.set_token(your_token)

df = ts.pro_bar(ts_code='600837.SH', adj='qfq', start_date='20080101', end_date='20191021')

print(df.to_csv())
df.to_csv('tsp_600837_SH.csv')