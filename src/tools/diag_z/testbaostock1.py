import baostock as bs
import pandas as pd

#### 登陆系统 ####
lg = bs.login()
# 显示登陆返回信息
print('login respond error_code:'+lg.error_code)
print('login respond  error_msg:'+lg.error_msg)

#### 获取沪深A股估值指标(日频)数据 ####
# peTTM    滚动市盈率
# psTTM    滚动市销率
# pcfNcfTTM    滚动市现率
# pbMRQ    市净率
#adjustflag：复权类型，默认不复权：3；1：后复权；2：前复权。已支持分钟线、日线、周线、月线前后复权。 BaoStock提供的是涨跌幅复权算法复权因子，具体介绍见：复权因子简介或者BaoStock复权因子简介。
#rs = bs.query_history_k_data_plus("sh.600837",
#rs = bs.query_history_k_data_plus("sh.601088",
rs = bs.query_history_k_data_plus("sz.000776",
#rs = bs.query_history_k_data_plus("sz.300059",
    "date,code,close,peTTM,pbMRQ,psTTM,pcfNcfTTM",
    start_date='2008-01-01', end_date='2020-02-05', 
    frequency="d", adjustflag="2")
print('query_history_k_data_plus respond error_code:'+rs.error_code)
print('query_history_k_data_plus respond  error_msg:'+rs.error_msg)

#### 打印结果集 ####
result_list = []
while (rs.error_code == '0') & rs.next():
    # 获取一条记录，将记录合并在一起
    result_list.append(rs.get_row_data())
result = pd.DataFrame(result_list, columns=rs.fields)

#### 结果集输出到csv文件 ####
result.to_csv("c:\\p3\\history_A_stock_valuation_indicator_data.csv", encoding="gbk", index=False)
print(result)

#### 登出系统 ####
bs.logout()
