from operator import itemgetter
import codecs
import datetime
import math




from operator import itemgetter
import codecs
import urllib
import urllib.request  
import re
import os
import time
import datetime
from sys import argv
import xlrd
from threading import Thread

from multiprocessing import cpu_count
import shutil


import baostock as bs
import pandas as pd

#li zi 1:
#http://www.sse.com.cn/market/sseindex/indexlist/constlist/index.shtml?COMPANY_CODE=000001&INDEX_Code=000001
#but can not get gu piao code....

#li zi 2:
#http://www.sse.com.cn/js/common/ssesuggestdata.js
#_t.push({val:"600030",val2:"中信证券",val3:"zxzq"});
#...
#it can use.


#li zi 3
#中证全指 web page
#http://www.csindex.com.cn/zh-CN/indices/index-detail/000985
#000985cons.xls
#http://www.csindex.com.cn/uploads/file/autofile/cons/000985cons.xls
#中证全指由全部A股股票中剔除ST、*ST股票，以及上市时间不足3个月等股票后的剩余股票构成样本股，具有较高的市场代表性。


#li zi 4
#中证A股指数由沪深两市全部A股组成，并剔除暂停上市的A股，指数以自由流通股本加权计算，综合反映A股上市股票价格的整体表现，具有较高的市场代表性，可作为投资标的和业绩评价基准。 
#http://www.csindex.com.cn/zh-CN/indices/index-detail/930903
#www.csindex.com.cn/uploads/file/autofile/cons/930903cons.xls
#it is very good!!!


#li zi 5
#http://www.csindex.com.cn/zh-CN/indices/index-detail/000985
#中证全指由全部A股股票中剔除ST、*ST股票，以及上市时间不足3个月等股票后的剩余股票构成样本股，具有较高的市场代表性。
#http://www.csindex.com.cn/uploads/file/autofile/cons/000985cons.xls

#li zi 6
#中证红利
#中证红利指数以沪深A股中现金股息率高、分红比较稳定、具有一定规模及流动性的100只股票为成分股，采用股息率作为权重分配依据，以反映A股市场高红利股票的整体表现。
#http://www.csindex.com.cn/zh-CN/indices/index-detail/000922
#http://www.csindex.com.cn/uploads/file/autofile/cons/000922cons.xls


#li zi 7
#中证医疗
#中证医疗指数选取与医疗器械、医疗服务等与医疗行业相关代表性公司股票作为指数样本股。采用自由流通调整市值加权，并根据成分股数量设置不同的权重上限，以反映沪深两市医疗主题股票的整体走势。
#http://www.csindex.com.cn/uploads/file/autofile/cons/399989cons.xls


#li zi 8
#中证医药
#中证医药卫生指数由中证800指数样本股中的医药卫生行业股票组成，以反映该行业公司股票的整体表现。
#该指数已纳入截至2018年9月30日的IOSCO金融基准原则鉴证报告范围。
#http://www.csindex.com.cn/uploads/file/autofile/cons/000933cons.xls




'''
上证红利指数挑选在上证所上市的现金股息率高、分红比较稳定、具有一定规模及流动性的50只股票作为样本，以反映上海证券市场高红利股票的整体状况和走势。 
http://www.csindex.com.cn/zh-CN/indices/index-detail/000015
'''
zzzs_code_default_0='000015'

'''
中证红利指数以沪深A股中现金股息率高、分红比较稳定、具有一定规模及流动性的100只股票为成分股，采用股息率作为权重分配依据，以反映A股市场高红利股票的整体表现。 
http://www.csindex.com.cn/zh-CN/indices/index-detail/000922
'''
zzzs_code_default_1='000922'


'''
中证全指证券公司指数选取中证全指样本股中至多50只证券公司行业股票组成，以反映该行业股票的整体表现。 
http://www.csindex.com.cn/zh-CN/indices/index-detail/399975
'''
zzzs_code_default_3='399975'

'''
上证50指数由沪市A股中规模大、流动性好的最具代表性的50只股票组成，反映上海证券市场最具影响力的一批龙头公司的股票价格表现。 
http://www.csindex.com.cn/zh-CN/indices/index-detail/000016
'''
zzzs_code_default_4='000016'

print('len(argv):',len(argv))
print('argv:',argv)
if len(argv)<2:
	zzzs_code_str=zzzs_code_default_3
else:
	zzzs_code_str=argv[1]



os_slash=os.sep
datafilepath = os.getcwd() + os_slash +"data"	+os_slash

if not os.path.exists(datafilepath):
	print("this path will be created,for save data",datafilepath);
	os.makedirs(datafilepath)






output_gupiao_code_list=[]



url_0c='http://www.csindex.com.cn/uploads/file/autofile/cons/%scons.xls'%(zzzs_code_str)

str_gupiao_code_list_excel_filename=datafilepath+"zzzs_gupiaocode_tempdata.xls"
print("access:%s,%s"%(url_0c,"............."));
urllib.request.urlretrieve(url_0c, str_gupiao_code_list_excel_filename)


data = xlrd.open_workbook(str_gupiao_code_list_excel_filename) # 打开xls文件
table1 = data.sheets()[0] # 打开第一张表
nrows = table1.nrows # 获取表的行数
the_output_string_lines_a1=[]
for i in range(nrows): # 循环逐行打印
	if i == 0: # 跳过第一行
		continue
	print(table1.row_values(i)[:6]) # 取前6列
	the_output_string_lines_a1.append(table1.row_values(i)[:6])
	str_gupiao_code = table1.row_values(i)[4]
	str_gupiao_name = table1.row_values(i)[5]	
	#print("%s is A gu."%(str_gupiao_code))
	output_gupiao_code_list.append([str_gupiao_code,str_gupiao_name])
print(len(output_gupiao_code_list))


#print(output_gupiao_code_list)
#print(len(output_gupiao_code_list))





print(datetime.datetime.now())



#input_file=codecs.open("a30.csv", 'r','utf-8')



table = []
#header = input_file.readline() 
#print(header)
gupiao_code_col_n=1;
amount_col_no=11;#
per_col_no=12;#
pb_col_no=13;#
minpb1_col_no=17
mktcap_col_no=14#
total_pb=0
sum_of_gupiao=0
total_mktcap_yi=0
total_pb_multi_mktcap_yi=0
total_amount_yi=0
total_pb_multi_amount_yi=0

total_minpb1=0
total_minpb1_multi_mktcap_yi=0
total_minpb1_multi_amount_yi=0


lg = bs.login()
if lg.error_code!='0':
        print('login respond error_code:'+lg.error_code)
        print('login respond  error_msg:'+lg.error_msg)
the_now=datetime.datetime.now()
print(the_now)
the_year=int(the_now.strftime("%Y"))
print(the_year)
the_month=int(the_now.strftime("%m"))
print(the_month)
the_day=int(the_now.strftime("%d"))
print(the_day)
the_today_date=str(the_year)+'-'+str(the_month)+'-'+str(the_day)
print(the_today_date)
print('gupiao_code','the_avg_pb','the_avg_minpb','the_avg_paomo')
print('-----------','---------','-------------','-------------')
result_list_of_total = []
for the_item in output_gupiao_code_list:
        the_code=the_item[0]
		#print(the_code)
        if the_code[0:1]=='6':
                the_code_str2='sh.'+the_code
        else:
                the_code_str2='sz.'+the_code
        rs = bs.query_history_k_data_plus(the_code_str2,
#            "date,code,close,peTTM,pbMRQ,psTTM,pcfNcfTTM",
                "pbMRQ",                                          
            start_date='2008-01-01', end_date='2020-02-03', 
            frequency="d", adjustflag="2")
        if lg.error_code!='0':
                print('query_history_k_data_plus respond error_code:'+rs.error_code)
                print('query_history_k_data_plus respond  error_msg:'+rs.error_msg)
        result_list = []
        while (rs.error_code == '0') & rs.next():
                tempstr=rs.get_row_data()
                #print(tempstr)
                try:
                        if float(tempstr[0])>0:
                                #print(tempstr[0])
                                result_list.append(float(tempstr[0]))
                except ValueError:
                        print("NOT a number！")
        #print(result_list)
        the_pb_now=result_list[-1]
        result_list.sort()
        lenofresult_list=len(result_list)
        theminpb_no_of_min2percent=int(lenofresult_list*0.02)
        min_of_result_list=result_list[theminpb_no_of_min2percent]
        #print(the_code,the_gp_name,the_pb_now,min_of_result_list,the_pb_now/min_of_result_list)
        the_gp_name=the_item[1]
        result_list_of_total.append([the_code,the_gp_name,the_pb_now,min_of_result_list,the_pb_now/min_of_result_list])
bs.logout()
the_avg_paomo=0
the_avg_paomo2=0
the_avg_minpb=0
the_avg_pb=0
the_cfg_num=len(result_list_of_total)
for ary_item1 in result_list_of_total:
        print(ary_item1)
        the_avg_pb=the_avg_pb+ary_item1[2]
        the_avg_minpb=the_avg_minpb+ary_item1[3]
        the_avg_paomo2=the_avg_paomo2++ary_item1[4]
the_avg_pb=the_avg_pb/the_cfg_num
the_avg_minpb=the_avg_minpb/the_cfg_num
the_avg_paomo=the_avg_pb/the_avg_minpb
the_avg_paomo2=the_avg_paomo2/the_cfg_num
print('the_cfg_num',the_cfg_num)
print(zzzs_code_str,'the_avg_pb','the_avg_minpb','the_avg_paomo')
print('------','----------','-------------','-------------')
print(zzzs_code_str,the_avg_pb,the_avg_minpb,the_avg_paomo)
print('the_avg_paomo2',the_avg_paomo2)

quit()
