from operator import itemgetter
import codecs
import urllib
import urllib.request  
import re
import os
import time
import datetime
import math
from sys import argv

from selenium import webdriver
import time
from selenium.webdriver.support.select import Select
from selenium.webdriver.chrome.options import Options


proxy_support = urllib.request.ProxyHandler({'http': '10.167.196.133:8080'})
opener = urllib.request.build_opener(proxy_support)
urllib.request.install_opener(opener)


div=2
allM=1000000/div
chromeOptions = webdriver.ChromeOptions()
chromeOptions.add_argument('--headless')
chromeOptions.add_argument('--ignore-certificate-errors')
chromeOptions.add_argument("--proxy-server=http://10.167.196.133:8080")
chrome_prefs = {}
chromeOptions.experimental_options["prefs"] = chrome_prefs
chrome_prefs["profile.default_content_settings"] = {"images": 2}
chrome_prefs["profile.managed_default_content_settings"] = {"images": 2}

print("#####A gu diag tool 0.2.0#####")
if len(argv)==1:
	str_gupiao_code='600837'
	print("notice:gu piao code is not specified,600837 will be used as default [gu piao code]!")
else:
	str_gupiao_code=argv[1]
print("An example of a call to this script:")
print("minpb [gu piao code]   #[gu piao code] is an integer such as 600837")
the_input_str=''
while the_input_str!='0':
	the_input_str = input("please input code：")
	if the_input_str=='0':
		break
	if the_input_str!='':
		str_gupiao_code=the_input_str

	if str_gupiao_code[0]=="6":
		shanghai_shenzhen_flag=0
		dwf_link_shanghai_shenzhen_flag="sh"
	else:
		shanghai_shenzhen_flag=1
		dwf_link_shanghai_shenzhen_flag="sz"	
	
	os_slash=os.sep
	if os.path.exists('/storage/sdcard1'): 
		datafilepath = '/storage/sdcard1/tempdata/'
	elif os.path.exists('/storage/sdcard0'): 
		datafilepath = '/storage/sdcard0/tempdata/'
	else:
		datafilepath = os.getcwd() + os_slash +"tempdata"	+os_slash
	
	print("this path will be created,for save data",datafilepath);
	if not os.path.exists(datafilepath):
		os.makedirs(datafilepath)
	
	the_now=datetime.datetime.now()
	print(the_now)
	the_year=int(the_now.strftime("%Y"))
	
	the_month=int(the_now.strftime("%m"))
	
	the_day=int(the_now.strftime("%d"))
	#------
	

	
	driver = webdriver.Chrome(chrome_options = chromeOptions)

	driver.implicitly_wait(0.001)

	driver.get("http://quote.eastmoney.com/{0}{1}.html".format(dwf_link_shanghai_shenzhen_flag,str_gupiao_code))
	print('debug 1:',"http://quote.eastmoney.com/{0}{1}.html".format(dwf_link_shanghai_shenzhen_flag,str_gupiao_code))
	text0A=''
	text0B=''	

	xpath = "//*[@id='{0}']".format('name')
	result = driver.find_element_by_xpath(xpath)
	text0A = result.text
	xpath = "//*[@id='{0}']".format('code')
	result = driver.find_element_by_xpath(xpath)
	text0B = result.text
	#print(text0B,text0A)
	
	the_output_result1=[]
	the_output_result2={}
	
	xpath = "//*[@data-bind='{0}']".format('pb')
	#xpath_pb = "//*[@id='{0}']".format('gt13')	
	result = driver.find_element_by_xpath(xpath)
	text1 = result.text
	the_output_result2['pb']=text1
	#print(the_output_result2)
	
	xpath = "//*[@data-bind='{0}']".format('pe')
	result = driver.find_element_by_xpath(xpath)
	text1 = result.text
	the_output_result2['pe']=text1
	#print(the_output_result2)

	xpath = "//*[@data-bind='{0}']".format('48')
	result = driver.find_element_by_xpath(xpath)
	text1 = result.text
	the_output_result2['amount']=text1
	#print(the_output_result2)	
	
	xpath = "//*[@data-bind='{0}']".format('43')
	result = driver.find_element_by_xpath(xpath)
	text1 = result.text
	the_output_result2['trade']=text1
	#print(the_output_result2)

	xpath = "//*[@data-bind='{0}']".format('changePercent')
	result = driver.find_element_by_xpath(xpath)
	text1 = result.text
	the_output_result2['changeofprice']=text1
	#print(the_output_result2)
	
	xpath = "//*[@data-bind='{0}']".format('change')
	result = driver.find_element_by_xpath(xpath)
	text1 = result.text
	the_output_result2['change']=text1
	#print(the_output_result2)	
	
	for k in range(1,100):

		xpath1 = "//li[@data-cc='{0}']".format(k)
		xpath_p = "//p[@data-cc='{0}']".format(k)
		xpath_td = "//p[@data-cc='{0}']".format(k)
		xpath_all = "//*[@data-bind='{0}']".format(k)	

		try:
			result1 = driver.find_element_by_xpath(xpath_all)
			text1 = result1.text
			#print('//* data-bind="{0}" :{1}'.format(k,text1))
			
			if k==48:#<td id="gt12" class="txtl" data-bind="48">-</td>
				text1_a=['成交额',text1]			
			elif k==43:#/td data-cc="5" :8.220  #need special #gu piao price of now 
				text1_a=['价格',text1]
			elif k==6:#<i data-cc="6" class="red" style="">+0.030</i>  #need special #gu piao price change(p) of today 
				text1_a=['价格变化元',text1]		
			elif k==7:#<i data-cc="7" class="red" style="">+0.37%</i>  #need special #gu piao price change(percent) of today 
				text1_a=['价格变化百分比',text1]		
			else:# the other
				text1_ary=text1.split(':')
				text1_a=[text1_ary[0],text1_ary[1]]
			the_output_result1.append(text1_a)
				
			#the_output_result1.append(text1_ary)
		except Exception as e:
			pass#print(e)
	#print('the_output_result1:')
	#print(the_output_result1)
	for item in  the_output_result1:
		if item[0]=='价格':
			the_output_result2['trade']=item[1]	#dividend yield
		if item[0]=='价格变化元':
			the_output_result2['changeofprice']=item[1]	
		if item[0]=='价格变化百分比':
			the_output_result2['change']=item[1]	
		if item[0]=='市净率':
			the_output_result2['pb']=item[1]
		if item[0]=='成交额':
			if item[1][-1]=='亿':
				the_output_result2['amount']=float(item[1][:-1])
			else:#'万'
				the_output_result2['amount']=float(item[1][:-1])/10000				
		if item[0]=='市净率':
			the_output_result2['pb']=item[1]
		if item[0]=='每股净资产':
			the_output_result2['b']=item[1]		
		if item[0]=='市盈率':
			the_output_result2['pe']=item[1]		
		if item[0]=='股息率':
			the_output_result2['dy']=item[1]	#dividend yield

	print(the_output_result2)
	driver.close()
	
	
	the_amount=the_output_result2['amount']
	
	the_cur_pb=float(the_output_result2['pb'])
	the_cur_pe=float(the_output_result2['pe'])
	#the_dy=the_output_result2['dy']
	the_dy='-'
	trade=float(the_output_result2['trade'])
	the_change=the_output_result2['change']
	
	
	
	
	
	#------
	cur_pb=the_cur_pb
	this_year=the_year
	
	look_years_num=this_year-2008
	
	
	start_year=this_year-look_years_num
	end_year=this_year-1
	str_start_day="%s0101"%(start_year)
	str_end_day="%s1231"%(end_year)
	str_gupiao_jz_filename=datafilepath+str_gupiao_code+"_jz"+".csv"
	code=str_gupiao_code

	output_file0=codecs.open(str_gupiao_jz_filename,'w+','utf-8')
	output_file0_header=["code","cur_pb"]
	print(output_file0_header)
	str_output_file0_header=",".join(output_file0_header)
	output_file0.write(str_output_file0_header)
	output_file0.write("\n")
	output_file0_line=[code,cur_pb]
	print(output_file0_line)

	output_file0_line_allstr=[]
	for x in output_file0_line:
		output_file0_line_allstr.append(str(x))
		
	str_output_file0_line=",".join(output_file0_line_allstr)
	output_file0.write(str_output_file0_line)
	output_file0.close()


	print("go to get the gu piao data of %s from %s to %s:"%(str_gupiao_code,str_start_day,str_end_day))
	url_1="http://quotes.money.163.com/service/chddata.html?code=%s%s&start=%s&end=%s"%(shanghai_shenzhen_flag,str_gupiao_code,str_start_day,str_end_day)

	url_2="http://quotes.money.163.com/service/zycwzb_%s.html?type=report"%(str_gupiao_code)

	str_gupiao_history_price_filename=datafilepath+str_gupiao_code+"_history_price"+".csv"
	if not os.path.exists(str_gupiao_history_price_filename):
		print("access:",url_1,".............");
		urllib.request.urlretrieve(url_1, str_gupiao_history_price_filename)


	str_gupiao_history_report_filename=datafilepath+str_gupiao_code+"_history_report"+".csv"
	if not os.path.exists(str_gupiao_history_report_filename):
		print("access:",url_2,".............");
		urllib.request.urlretrieve(url_2, str_gupiao_history_report_filename)



	str_gupiao_minpb2_filename=datafilepath+str_gupiao_code+"_minpb2"+".csv"

	if 1 or not os.path.exists(str_gupiao_minpb2_filename):
		


		date_history=[]
		tmv_history=[]
		bv_history=[]
		pb_history=[]
		net_profit_history=[]
		total_assets_history=[]
		avg_roa=0

		one_year_month_number=4
		look_years_num=look_years_num 
		look_report_num=4*look_years_num

		for y in range(1,look_years_num+1):
			for m in range(0,one_year_month_number):
				temp_year_str=str(this_year-y)
				temp_month_str="%02d"%(12-m*3)
				if temp_month_str=="12":
					temp_day_str="31"
				elif temp_month_str=="03":
					temp_day_str="31"
				else: ##06,09
					temp_day_str="30"		
				temp_date_str="-".join([temp_year_str,temp_month_str,temp_day_str])
				date_history.append(temp_date_str);
			





		str_today=time.strftime('%Y-%m-%d',time.localtime(time.time()))


		print("-----")

		date_row_n=0
		bv_row_n=18
		net_profit_row_n=10
		total_assets_row_n=14
		input_file2=codecs.open(str_gupiao_history_report_filename, 'r','gbk')#try gb2312 -> gbk?
		k=0

		for line in input_file2:
			if (k==date_row_n):
				temp_date_line=line.split(",")
			if (k==bv_row_n):
				temp_bv_line=line.split(",")	

			k=k+1


		k=0
		for k in range(0,look_report_num):
			tmv_history.append("-")
			bv_history.append("-")	
			pb_history.append("-")

		
		len_of_temp_date_line=len(temp_date_line)
		
		if len_of_temp_date_line<4:
			print("jump gen the minpb of ci xin gu!")#TODO:
			exit()
		
		k=0
		flag_hit=0
		for one_col in temp_date_line:
			if date_history[0]==one_col:
				flag_hit=1
				break;
			k=k+1
		
		col_offset=0
		if k==1:
			col_offset=1
		if k==2:
			col_offset=2
		if k==3:
			col_offset=3	
		if k==4:
			if flag_hit==1:
				col_offset=4	
		
		k=0
		for one_col in temp_bv_line:

			
			if k>0:


				
				if k-col_offset>=0 and k-col_offset<look_report_num:


			
					if col_offset==0:
						bv_history[k-col_offset]=one_col
				


					if col_offset==1:
						bv_history[k-col_offset]=one_col

						
						
					if col_offset==2:
						bv_history[k-col_offset]=one_col

						
					if col_offset==3:
						bv_history[k-col_offset]=one_col

						
					if col_offset==4:
						bv_history[k-col_offset]=one_col


			k=k+1
			
		input_file2.close()


		input_file1=codecs.open(str_gupiao_history_price_filename, 'r','gbk')#try gb2312 -> gbk?

		str_input_file1_header=input_file1.readline()
		strlist_input_file1_header=str_input_file1_header.split(",")


		tmv_col_n=13

		prev_dt_tmpline0=str(this_year)+"-01-01"




		k=0
		the_days_after_ipo=0
		gupiao_name=""
		gupiao_name_col_n=2
		for line in input_file1:
			the_days_after_ipo=the_days_after_ipo+1
			tmpline=line.split(",")
			for t in date_history:
				dt_tmpline0=tmpline[0]
				dt_t=t
				if (dt_t<prev_dt_tmpline0 and dt_t>=dt_tmpline0):

					tmv_history[k]=tmpline[tmv_col_n];	
					k=k+1
					if gupiao_name=="":
						gupiao_name=tmpline[gupiao_name_col_n]
					if k>=look_report_num:
						break
			if k>=look_report_num:
				break
			prev_dt_tmpline0=dt_tmpline0		

		input_file1.close()


		k=0
		for k in range(0,look_report_num):
			if tmv_history[k]=="-":
				continue
			if bv_history[k]=="-":
				continue
			f_tmv = float(tmv_history[k])
			if bv_history[k]=="--":
				continue
			f_bv = float(bv_history[k])
			if not f_bv==0.0:
				pb_history[k]=round(f_tmv/10000/f_bv,3)
		
		if bv_history[0]=='-':
			if tmv_history[0]!="-" and bv_history[1]!="-" and bv_history[1]!="--":
				f_tmv_0 = float(tmv_history[0])
				f_bv_0 = float(bv_history[1])
				if not f_bv==0.0:
					pb_history[0]=round(f_tmv_0/10000/f_bv_0,3)




	print("####output reuslt####")
	print("today:",str_today)
	
	print("------------------------")
	
	print("[10 year pb history]")
	print("date          pb")
	print("----------    ----------")


	k=0
	#date_history_reverse=list(reversed(date_history))
	#print(date_history)
	date_history_reverse=date_history
	for one_item in date_history_reverse:
		if pb_history[k]=="-":
			temp_str_pb="%-10s"%"-"
		else:
			temp_str_pb="%-10.03f"%(pb_history[k])
		print("%s    %s"%(one_item,temp_str_pb))
		k=k+1

	
	print("[min pb]")
	the_minpb=0.0
	pb_history_del_null_for_min=[]
	k=0
	for temppb in pb_history:
		if not temppb=="-":
			pb_history_del_null_for_min.append(temppb)
			k=k+1
	if pb_history_del_null_for_min==[]:
		print("[waring,This gupiao has NO history pb for last year!!!!]")
		exit()
	else:
		this_func_result1="%-10.03f"%(min(pb_history_del_null_for_min))
		print(this_func_result1)
		the_minpb=float(this_func_result1)
	


	

	'''
	#this is test data
	the_amount=0.75562
	the_minpb=0.5
	the_cur_pb=0.6
	the_cur_pe=10.960
	trade=8.220
	the_change=0.37
	the_dy=2.7
	'''
	#print('the_amount={0} yi'.format(the_amount))
	#print('the_minpb={0}'.format(the_minpb))
	#print('the_cur_pb={0}'.format(the_cur_pb))
	#print('the_cur_pe={0}'.format(the_cur_pe))
	#print('the_dy={0}'.format(the_dy))
	#print('trade={0}'.format(trade))
	#print('the_change={0}'.format(the_change))

	#
	output_gupiao_code_list=[str_gupiao_code]
	
	str_gupiao_sn_filename=datafilepath+'the_gupiao_data'+"_sn"+".html"
	url_sn0='http://hq.sinajs.cn/list='
	url_sn1=''
	output_sn_gupiao_data_list=[]
	output_a00_beiyong_csv_data_str_line_list=[]
	k=0
	len_of_output_gupiao_code_list=len(output_gupiao_code_list)
	for a_gupiao_code in output_gupiao_code_list:
		str_gupiao_code=a_gupiao_code
		if str_gupiao_code[0]=='6':
			the_shanghai_shenzhen_flag='sh'
		else:
			the_shanghai_shenzhen_flag='sz'
		url_sn1=url_sn1+the_shanghai_shenzhen_flag+str_gupiao_code+','
		k=k+1
		if k%500==0 or k==len_of_output_gupiao_code_list:		

			urllib.request.urlretrieve(url_sn0+url_sn1, str_gupiao_sn_filename)
			input_sn_file1=codecs.open(str_gupiao_sn_filename, 'r','GB18030')
			for line in input_sn_file1:
				output_sn_gupiao_data_list.append(line)
			url_sn1=''

	trade=0
	
	k=0
	the_output2_lines=[]
	
	the_price='-'
	the_change='-'
	for str_line in output_sn_gupiao_data_list:
		k=k+1

		str_gupiao_code=str_line[13:19]

		sub_str1=str_line[21:-3]

		cols_p1 =[str(k),str_gupiao_code]
		cols_p2 = sub_str1.split(',')

		cols_all=cols_p1+cols_p2

		the_output2_lines.append(cols_all)

	trade=float(the_output2_lines[0][5])
	the_amount=float(the_output2_lines[0][11])
	the_amount_outputstr='%8.2f yi'%(round(the_amount*1.0/100000000,2))
	#the_price=trade
	the_change=(float(the_output2_lines[0][5])-float(the_output2_lines[0][4]))/float(the_output2_lines[0][4])*100	
	
	#
	print(text0B,text0A)
	if the_cur_pb<=0 or the_cur_pe<=0:
		print("[current pb or current pe <=0 ! ] mean this gu piao kuai po can le! do NOT buy this gu piao]")
		continue

	if 0 and the_amount<0.001:
		print("[amount<0.001 yi ! ] mean this gu piao is too cold , do NOT buy this gu piao]")
		continue
	else:
		print("[the min pb]")
		print('%10.2f'%(the_minpb))
		print("[current pb]")
		#print(cur_pb)
		the_cur_pb=float(the_cur_pb)
		print('%10.2f'%(the_cur_pb))
		print("[pb pao mo]")
		the_pb_paomo=round(the_cur_pb/the_minpb,2)
		print('%10.3f'%(the_pb_paomo))

		print("[risk level]")
		if the_pb_paomo<=1.1:
			print('     low risk')
		elif the_pb_paomo<=1.5:
			print('     middle risk')
		else:
			print('     high risk')

		print('[dividend yield]')
		print('     {}'.format(the_dy))
		
		print('[zhi neng cang wei]')

		ratio_of_pb_to_minpb2=the_pb_paomo
		#a_buy_of_per_wang_ge=3000*2
		a_buy_of_per_wang_ge=allM/20
		zhi_neng_cang_wei=0
		max_cang_wei_per_stock=(32+0)*a_buy_of_per_wang_ge #not jin zi ta
		if ratio_of_pb_to_minpb2<=0.3:
			jin_zi_ta_l_level=32
		elif ratio_of_pb_to_minpb2>=1.5:
			jin_zi_ta_l_level=0
		else:
			jin_zi_ta_l_level=math.log((ratio_of_pb_to_minpb2/1.5),0.95)

		if jin_zi_ta_l_level<=0:
			jin_zi_ta_l_level=0

		zhi_neng_cang_wei=(0+jin_zi_ta_l_level)*a_buy_of_per_wang_ge
		gupiao_code=str_gupiao_code


		output_gupiao_code_list=[str_gupiao_code]





		if trade==0:
			zhi_neng_gu_fen=0
		else:
			zhi_neng_gu_fen=zhi_neng_cang_wei*1.0/trade


		zhi_neng_cang_wei=math.floor(zhi_neng_cang_wei/1000)
		zhi_neng_cang_wei=zhi_neng_cang_wei/10
		if zhi_neng_cang_wei<=0:
			zhi_neng_cang_wei=0
		str_zhi_neng_cang_wei='%3.1fw'%(round(zhi_neng_cang_wei,1))


		zhi_neng_gu_fen = int(zhi_neng_gu_fen*1.0/100)*100
		if zhi_neng_gu_fen<=0:
			zhi_neng_gu_fen=0
		str_zhi_neng_gu_fen=str(zhi_neng_gu_fen)+'gu'
		str_zhi_neng_cang_wei=str_zhi_neng_cang_wei+'['+str_zhi_neng_gu_fen+']'
		print('     %s'%(str_zhi_neng_cang_wei))
		print('[stock price]')
		print('%10.3f(%10.3f%%)  %s'%(trade,round(the_change,2),the_amount_outputstr))

driver.quit()