from operator import itemgetter
import codecs
import urllib
import urllib.request  
import re
import os
import time
import datetime
from sys import argv
import xlrd
from threading import Thread
from multiprocessing import Process
from multiprocessing import cpu_count
import shutil
import tarfile
import lzma





os_slash=os.sep
datafilepath = os.getcwd() + os_slash +"data"	+os_slash




the_now=datetime.datetime.now()
print(the_now)
the_year=int(the_now.strftime("%Y"))
print(the_year)
the_month=int(the_now.strftime("%m"))
print(the_month)
#the_day=int(the_now.strftime("%d"))
#print(the_day)

os_slash=os.sep
data_history_filepath_thismonth = os.getcwd() + os_slash +'data_history'+os_slash+str(the_year)+'_'+str(the_month)+os_slash
data_history_filepath_thisyear = os.getcwd() + os_slash +'data_history_year'+os_slash+str(the_year)+os_slash

if not os.path.exists(data_history_filepath_thismonth):
	print('this path of this month[%s]will be created,for save data'%(data_history_filepath_thismonth))
	os.makedirs(data_history_filepath_thismonth)

if not os.path.exists(data_history_filepath_thisyear):
	print('this path of this year[%s]will be created,for save data'%(data_history_filepath_thisyear))
	os.makedirs(data_history_filepath_thisyear)



for a_file in os.listdir(datafilepath):
	if os.path.splitext(a_file)[1] == '.csv':
		print(a_file)
		r=shutil.copy(os.path.join(datafilepath,a_file),os.path.join(data_history_filepath_thismonth,a_file))
		print('copy path is '+r)

for a_file in os.listdir(datafilepath):
	if the_month == 12:	
		print(a_file)
		r=shutil.copy(os.path.join(datafilepath,a_file),os.path.join(data_history_filepath_thisyear,a_file))
		print('copy path is '+r)



datafilepath_output1 = os.path.expanduser('~') + os_slash +'lhjz_history_data'
if not os.path.exists(datafilepath_output1):
	print('this path of [%s]will be created,for save compressed data'%(datafilepath_output1))
	os.makedirs(datafilepath_output1)

datafilepath_output2 = os.path.expanduser('~') + os_slash +'lhjz_history_data'
if not os.path.exists(datafilepath_output2):
	print('this path of [%s]will be created,for save compressed data'%(datafilepath_output2))
	os.makedirs(datafilepath_output2)

TAR_XZ_FILENAME = datafilepath_output1+os_slash+str(the_year)+'_'+str(the_month)+'.tar.xz'
#DIRECTORY_NAME = data_history_filepath_thismonth
DIRECTORY_NAME = 'data_history'
#os.remove(TAR_XZ_FILENAME)
xz_file = lzma.LZMAFile(TAR_XZ_FILENAME, mode='w')

with tarfile.open(mode='w', fileobj=xz_file) as tar_xz_file:
    tar_xz_file.add(DIRECTORY_NAME)

xz_file.close()

if the_month == 12:
	TAR_XZ_FILENAME = datafilepath_output2+os_slash+str(the_year)+'.tar.xz'
	#DIRECTORY_NAME = data_history_filepath_thisyear
	DIRECTORY_NAME = 'data_history_year'
	#os.remove(TAR_XZ_FILENAME)
	xz_file = lzma.LZMAFile(TAR_XZ_FILENAME, mode='w')

	with tarfile.open(mode='w', fileobj=xz_file) as tar_xz_file:
	    tar_xz_file.add(DIRECTORY_NAME)

	xz_file.close()
	


