from operator import itemgetter
import codecs
import urllib
import urllib.request  
import re
import os
import time
import datetime
from sys import argv
import xlrd
from threading import Thread

from multiprocessing import cpu_count
import shutil

#li zi 1:
#http://www.sse.com.cn/market/sseindex/indexlist/constlist/index.shtml?COMPANY_CODE=000001&INDEX_Code=000001
#but can not get gu piao code....

#li zi 2:
#http://www.sse.com.cn/js/common/ssesuggestdata.js
#_t.push({val:"600030",val2:"中信证券",val3:"zxzq"});
#...
#it can use.


#li zi 3
#中证全指 web page
#http://www.csindex.com.cn/zh-CN/indices/index-detail/000985
#000985cons.xls
#http://www.csindex.com.cn/uploads/file/autofile/cons/000985cons.xls
#中证全指由全部A股股票中剔除ST、*ST股票，以及上市时间不足3个月等股票后的剩余股票构成样本股，具有较高的市场代表性。


#li zi 4
#中证A股指数由沪深两市全部A股组成，并剔除暂停上市的A股，指数以自由流通股本加权计算，综合反映A股上市股票价格的整体表现，具有较高的市场代表性，可作为投资标的和业绩评价基准。 
#http://www.csindex.com.cn/zh-CN/indices/index-detail/930903
#www.csindex.com.cn/uploads/file/autofile/cons/930903cons.xls
#it is very good!!!


#li zi 5
#http://www.csindex.com.cn/zh-CN/indices/index-detail/000985
#中证全指由全部A股股票中剔除ST、*ST股票，以及上市时间不足3个月等股票后的剩余股票构成样本股，具有较高的市场代表性。
#http://www.csindex.com.cn/uploads/file/autofile/cons/000985cons.xls

os_slash=os.sep
datafilepath = os.getcwd() + os_slash +"data"	+os_slash

if not os.path.exists(datafilepath):
	print("this path will be created,for save data",datafilepath);
	os.makedirs(datafilepath)







#www.csindex.com.cn/uploads/file/autofile/cons/930903cons.xls
output_gupiao_code_list=[]

#中证全指证券公司指数
url_0c='http://www.csindex.com.cn/uploads/file/autofile/cons/399975cons.xls'

str_gupiao_code_list_excel_filename=datafilepath+"gupiaocode_data"+".xls"
print("access:%s,%s"%(url_0c,"............."));
urllib.request.urlretrieve(url_0c, str_gupiao_code_list_excel_filename)


data = xlrd.open_workbook(str_gupiao_code_list_excel_filename) # 打开xls文件
table1 = data.sheets()[0] # 打开第一张表
nrows = table1.nrows # 获取表的行数
for i in range(nrows): # 循环逐行打印
	if i == 0: # 跳过第一行
		continue
	print(table1.row_values(i)[:6]) # 取前6列
	str_gupiao_code = table1.row_values(i)[4]
	print("%s is A gu."%(str_gupiao_code))
	output_gupiao_code_list.append(str_gupiao_code)



print(output_gupiao_code_list)
print(len(output_gupiao_code_list))


str_gupiao_code_list_filename=datafilepath+"gupiao_code_list.txt"
fo = open(str_gupiao_code_list_filename, "w")

print("文件名为: ",fo.name)
fo.write("code")
fo.write("\n")
for str_item in output_gupiao_code_list:
	fo.write( str_item )
	fo.write( "\n" )

# 关闭文件
fo.close()







def get_gupiao_curpb(str_gupiao_code,the_3jlt_list_dcw):
	#global the_3jlt_list_dcw
	if not len(str_gupiao_code)==6:
		print("error, invalid gu piao code: %s !"%(str_gupiao_code))
		return

	if str_gupiao_code[0]=="6":
		shanghai_shenzhen_flag=0
		dwf_link_shanghai_shenzhen_flag="sh"
	else:
		shanghai_shenzhen_flag=1
		dwf_link_shanghai_shenzhen_flag="sz"
	
	url_3="http://quote.eastmoney.com/%s%s.html"%(dwf_link_shanghai_shenzhen_flag,str_gupiao_code)	
	str_gupiao_dcw_filename=datafilepath+str_gupiao_code+"_dcw"+".html"
	if not os.path.exists(str_gupiao_dcw_filename):
		print("access:",url_3,".............");
		urllib.request.urlretrieve(url_3, str_gupiao_dcw_filename)
		#time.sleep(0.1)	
	
	str_gupiao_dcw_filename=datafilepath+str_gupiao_code+"_dcw"+".html"
	#judge if it is a ban kuai long tou
	if os.path.exists(str_gupiao_dcw_filename):
		#input_dcw_file1=codecs.open(str_gupiao_dcw_filename, 'r','gb2312')
		input_dcw_file1=codecs.open(str_gupiao_dcw_filename, 'r','GB18030')
		#                                                          总市值 	    净资产 	        净利润
		#to seek the line include this:<td><b>行业排名</b></td>	<td>1|39</td>	<td>1|39</td>	<td>1|39</td>
		str_pattern0a='target=_blank><b>'#target=_blank><b>中信证券</b>
		str_pattern0b='target="_blank">'#target="_blank">券商信托</a>
		
		str_pattern1='<td><b>行业排名</b></td>'
		str_pattern2='<td>'
		str_pattern3='<td>'
		str_pattern4='<td>'	
		len_str_pattern1=len(str_pattern1)
		for line in input_dcw_file1:
			str_line=line.strip()
			seek_offset1=str_line.find(str_pattern1)
			if (not seek_offset1==-1):
				sub_str1=str_line[seek_offset1+len_str_pattern1:]
				#print(sub_str1)
				seek_offset2=sub_str1.find(str_pattern2)
				seek_offset3begin=sub_str1.find(str_pattern3,seek_offset2+len(str_pattern2))
				seek_offset3end=sub_str1.find('|',seek_offset3begin)
				sub_str3=sub_str1[seek_offset3begin+len(str_pattern3):seek_offset3end]
				#print(sub_str3)
				
				seek_offset4begin=sub_str1.find(str_pattern4,seek_offset3begin+len(str_pattern3))				
				seek_offset4end=sub_str1.find('|',seek_offset4begin)
				sub_str4=sub_str1[seek_offset4begin+len(str_pattern4):seek_offset4end]
				#print(sub_str4)
				
				str_jingzichan_paiming=sub_str3

				
				str_jinglirun_paiming=sub_str4
				if str_jingzichan_paiming.isdigit() and str_jinglirun_paiming.isdigit():
					#if (int(str_jingzichan_paiming)+int(str_jinglirun_paiming))<=4:
					if int(str_jingzichan_paiming)==1 or int(str_jinglirun_paiming)<=10:
						seek_offset0a=str_line.find(str_pattern0a)
						seek_offset0aend=str_line.find('</b>',seek_offset0a)
						str_gupiao_name=str_line[seek_offset0a+len(str_pattern0a):seek_offset0aend]
						#print(str_gupiao_name)
						
						seek_offset0b=str_line.find(str_pattern0b)
						seek_offset0bend=str_line.find('</a>',seek_offset0b)
						str_gupiao_bankuai=str_line[seek_offset0b+len(str_pattern0b):seek_offset0bend]
						#print(str_gupiao_bankuai)
						the_quan_offset=str_gupiao_bankuai.find('券')
						if the_quan_offset!=-1:
							the_3jlt_list_dcw.append([str_gupiao_code,str_gupiao_name,str_gupiao_bankuai,str_jingzichan_paiming,str_jinglirun_paiming]);
						#print(the_3jlt_list_dcw)
						#print([str_gupiao_code,str_jingzichan_paiming,str_jinglirun_paiming])
				break	
		input_dcw_file1.close()


def get_gupiao_list_curpb(gupiao_code_list,startk,endk,the_3jlt_list_dcw):
	print('sub thread pid',os.getpid())
	for k in range(startk,endk):
		str_item=gupiao_code_list[k]
		str_gupiao_code=str_item
		get_gupiao_curpb(str_gupiao_code,the_3jlt_list_dcw)


		
the_3jlt_list_dcw=[]

item_total_num=len(output_gupiao_code_list)
print(item_total_num)
#the_cpu_num=cpu_count()*10
the_cpu_num=1
print(the_cpu_num)

the_shang=item_total_num//the_cpu_num
the_yushu=item_total_num%the_cpu_num

my_thread_list=[]

for cpu_k in range(0,the_cpu_num):
	startk=the_shang*cpu_k
	endk=the_shang*(cpu_k+1)
	print(item_total_num,cpu_k,the_shang,the_yushu,startk,endk)
	pk=Thread(target=get_gupiao_list_curpb, args=(output_gupiao_code_list,startk,endk,the_3jlt_list_dcw))
	my_thread_list.append(pk)
	pk.start()

if 	the_yushu>0:
	startk=the_shang*the_cpu_num
	endk=item_total_num
	print(item_total_num,cpu_k,the_shang,the_yushu,startk,endk)
	pk=Thread(target=get_gupiao_list_curpb, args=(output_gupiao_code_list,startk,endk,the_3jlt_list_dcw))
	my_thread_list.append(pk)
	pk.start()
	
for pk in my_thread_list:
	pk.join()
	pass


print('item_total_num:',item_total_num)
print('the_cpu_num:',the_cpu_num)
print('main process end',os.getpid())

for item in the_3jlt_list_dcw:
	print(item)
print(len(the_3jlt_list_dcw))


str_gupiao_3jlt_dcw_filename=datafilepath+"gupiao_3jlt_dcw"+".csv"

output_file0=codecs.open(str_gupiao_3jlt_dcw_filename,'w+','utf-8')
output_file0_header=['gupiao_code','gupiao_name','gupiao_bankuai','jingzichan_paiming','jinglirun_paiming']
print(output_file0_header)
str_output_file0_header=",".join(output_file0_header)
output_file0.write(str_output_file0_header)


for item in the_3jlt_list_dcw:
	output_file0_line_allstr=item
	str_output_file0_line=",".join(output_file0_line_allstr)
	print(str_output_file0_line)
	output_file0.write("\n")
	output_file0.write(str_output_file0_line)
output_file0.close()

shutil.copy(str_gupiao_3jlt_dcw_filename, '..'+os_slash)


	
	
	

	


