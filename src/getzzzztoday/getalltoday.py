from operator import itemgetter
import codecs
import urllib
import urllib.request  
import re
import os
import time
import datetime
from sys import argv
import xlrd
from threading import Thread
from multiprocessing import Process
from multiprocessing import cpu_count
import shutil





#li zi 1:
#http://www.sse.com.cn/market/sseindex/indexlist/constlist/index.shtml?COMPANY_CODE=000001&INDEX_Code=000001
#but can not get gu piao code....

#li zi 2:
#http://www.sse.com.cn/js/common/ssesuggestdata.js
#_t.push({val:"600030",val2:"中信证券",val3:"zxzq"});
#...
#it can use.


#li zi 3
#中证全指 web page
#http://www.csindex.com.cn/zh-CN/indices/index-detail/000985
#000985cons.xls
#http://www.csindex.com.cn/uploads/file/autofile/cons/000985cons.xls
#中证全指由全部A股股票中剔除ST、*ST股票，以及上市时间不足3个月等股票后的剩余股票构成样本股，具有较高的市场代表性。


#li zi 4
#中证A股指数由沪深两市全部A股组成，并剔除暂停上市的A股，指数以自由流通股本加权计算，综合反映A股上市股票价格的整体表现，具有较高的市场代表性，可作为投资标的和业绩评价基准。 
#http://www.csindex.com.cn/zh-CN/indices/index-detail/930903
#www.csindex.com.cn/uploads/file/autofile/cons/930903cons.xls
#it is very good!!!


#li zi 5
#http://www.csindex.com.cn/zh-CN/indices/index-detail/000985
#中证全指由全部A股股票中剔除ST、*ST股票，以及上市时间不足3个月等股票后的剩余股票构成样本股，具有较高的市场代表性。
#http://www.csindex.com.cn/uploads/file/autofile/cons/000985cons.xls

os_slash=os.sep
datafilepath = os.getcwd() + os_slash +"data"	+os_slash




the_now=datetime.datetime.now()
print(the_now)
the_year=int(the_now.strftime("%Y"))
print(the_year)
the_month=int(the_now.strftime("%m"))
print(the_month)
#the_day=int(the_now.strftime("%d"))
#print(the_day)

os_slash=os.sep
data_history_filepath_thismonth = os.getcwd() + os_slash +'data_history'+os_slash+str(the_year)+'_'+str(the_month)+os_slash
data_history_filepath_thisyear = os.getcwd() + os_slash +'data_history_year'+os_slash+str(the_year)+os_slash

if not os.path.exists(data_history_filepath_thismonth):
	print('this path of this month[%s]will be created,for save data'%(data_history_filepath_thismonth))
	os.makedirs(data_history_filepath_thismonth)

if not os.path.exists(data_history_filepath_thisyear):
	print('this path of this year[%s]will be created,for save data'%(data_history_filepath_thisyear))
	os.makedirs(data_history_filepath_thisyear)


'''
if not os.path.exists(datafilepath):
	print("this path will be created,for save data",datafilepath);
	os.makedirs(datafilepath)

#url_0="http://www.sse.com.cn/market/sseindex/indexlist/constlist/index.shtml?COMPANY_CODE=000001&INDEX_Code=000001"
#str_gupiao_sz_code_list_filename=datafilepath+"sz_code_list"+".html"
url_0="http://www.sse.com.cn/js/common/ssesuggestdata.js"
str_gupiao_sz_code_list_filename=datafilepath+"sz_gupiaocode_suggestdata"+".js"
print("access:%s,%s"%(url_0,"............."));
urllib.request.urlretrieve(url_0, str_gupiao_sz_code_list_filename)

output_gupiao_code_list=[]


if os.path.exists(str_gupiao_sz_code_list_filename):
	input_gupiao_code_list_file1=codecs.open(str_gupiao_sz_code_list_filename, 'r','utf-8')
	#to seek the line like this:
	#_t.push({val:"600030",val2:"中信证券",val3:"zxzq"});
	str_pattern1="_t.push({val:\""
	str_pattern2="\","
	len_str_pattern1=len(str_pattern1)
	for line in input_gupiao_code_list_file1:
		str_line=line.strip()
		seek_offset1=str_line.find(str_pattern1)
		if (not seek_offset1==-1):
			sub_str1=str_line[seek_offset1+len_str_pattern1:]
			print(sub_str1)
			seek_offset2=sub_str1.find(str_pattern2)
			sub_str2=sub_str1[0:seek_offset2]
			print(sub_str2)
			str_gupiao_code=sub_str2
			print("%s"%(str_gupiao_code))			
			if not len(str_gupiao_code)==6:
				print("in gen_minpb2_and_save() ,error, invalid gu piao code: %s !"%(str_gupiao_code))
				continue	
			if str_gupiao_code[0]=="6":
				print("%s is A gu."%(str_gupiao_code))
				output_gupiao_code_list.append(str_gupiao_code)
			else:
				print("%s is NOT A gu."%(str_gupiao_code))			
		

	input_gupiao_code_list_file1.close()



#http://www.szse.cn/szseWeb/ShowReport.szse?SHOWTYPE=xlsx&CATALOGID=1747&ZSDM=399106&tab1PAGENO=1&ENCODE=1&TABKEY=tab1
#it is xlsx file 
url_0b="http://www.szse.cn/szseWeb/ShowReport.szse?SHOWTYPE=xlsx&CATALOGID=1747&ZSDM=399106&tab1PAGENO=1&ENCODE=1&TABKEY=tab1"
str_gupiao_sz_code_list_excel_filename=datafilepath+"sz_gupiaocode_data"+".xlsx"
print("access:%s,%s"%(url_0b,"............."));
urllib.request.urlretrieve(url_0b, str_gupiao_sz_code_list_excel_filename)


data = xlrd.open_workbook(str_gupiao_sz_code_list_excel_filename) # 打开xls文件
table1 = data.sheets()[0] # 打开第一张表
nrows = table1.nrows # 获取表的行数
for i in range(nrows): # 循环逐行打印
	if i == 0: # 跳过第一行
		continue
	print(table1.row_values(i)[:5]) # 取前5列
	str_gupiao_code = table1.row_values(i)[0]
	if str_gupiao_code[0]=="0" or str_gupiao_code[0]=="3":
		print("%s is A gu."%(str_gupiao_code))
		output_gupiao_code_list.append(str_gupiao_code)
	else:
		print("%s is NOT A gu."%(str_gupiao_code))	


print(output_gupiao_code_list)
print(len(output_gupiao_code_list))


str_gupiao_code_list_filename=datafilepath+"gupiao_code_list_beiyong.txt"
fo = open(str_gupiao_code_list_filename, "w")

print("文件名为: ",fo.name)
fo.write("code")
fo.write("\n")
for str_item in output_gupiao_code_list:
	fo.write( str_item )
	fo.write( "\n" )

# 关闭文件
fo.close()
'''


#中证A股
#www.csindex.com.cn/uploads/file/autofile/cons/930903cons.xls
output_gupiao_code_list=[]
#url_0c="http://www.csindex.com.cn/uploads/file/autofile/cons/930903cons.xls"

'''
中证全指证券公司指数选取中证全指样本股中至多50只证券公司行业股票组成，以反映该行业股票的整体表现。 
http://www.csindex.com.cn/zh-CN/indices/index-detail/399975
'''
url_0c='http://www.csindex.com.cn/uploads/file/autofile/cons/399975cons.xls'

str_gupiao_code_list_excel_filename=datafilepath+"gupiaocode_data"+".xls"
print("access:%s,%s"%(url_0c,"............."));
urllib.request.urlretrieve(url_0c, str_gupiao_code_list_excel_filename)


data = xlrd.open_workbook(str_gupiao_code_list_excel_filename) # 打开xls文件
table1 = data.sheets()[0] # 打开第一张表
nrows = table1.nrows # 获取表的行数
for i in range(nrows): # 循环逐行打印
	if i == 0: # 跳过第一行
		continue
	print(table1.row_values(i)[:6]) # 取前6列
	str_gupiao_code = table1.row_values(i)[4]
	print("%s is A gu."%(str_gupiao_code))
	output_gupiao_code_list.append(str_gupiao_code)



print(output_gupiao_code_list)
print(len(output_gupiao_code_list))


str_gupiao_code_list_filename=datafilepath+"gupiao_code_list.txt"
fo = open(str_gupiao_code_list_filename, "w")

print("文件名为: ",fo.name)
fo.write("code")
fo.write("\n")
for str_item in output_gupiao_code_list:
	fo.write( str_item )
	fo.write( "\n" )

# 关闭文件
fo.close()








def get_gupiao_curpb(str_gupiao_code):
	if not len(str_gupiao_code)==6:
		print("error, invalid gu piao code: %s !"%(str_gupiao_code))
		return

	if str_gupiao_code[0]=="6":
		shanghai_shenzhen_flag=0
		dwf_link_shanghai_shenzhen_flag="sh"
	else:
		shanghai_shenzhen_flag=1
		dwf_link_shanghai_shenzhen_flag="sz"
	
	url_3="http://quote.eastmoney.com/%s%s.html"%(dwf_link_shanghai_shenzhen_flag,str_gupiao_code)	
	str_gupiao_dcw_filename=datafilepath+str_gupiao_code+"_dcw"+".html"
	if not os.path.exists(str_gupiao_dcw_filename):
		print("access:",url_3,".............");
		urllib.request.urlretrieve(url_3, str_gupiao_dcw_filename)
		#time.sleep(0.1)	

	'''
				<table class="line23 w100p bt txtUL" id="rtp2" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td><a href="http://data.eastmoney.com/bbsj/600276.html" target="_blank">收益</a>(<span title="第三季度">三</span>)：0.791</td>
							<td>PE(动)：<span id="gt6_2">55.96</span></td>
						</tr>
						<tr>
							<td><a href="http://data.eastmoney.com/bbsj/600276.html" target="_blank">净资产</a>：5.039</td>
							<td>市净率：<span id="gt13_2">11.71</span></td>
						</tr>
						<tr>
							<td>营收：124.6亿</td>
							<td><a href="http://data.eastmoney.com/bbsj/600276.html" target="_blank">同比</a>：23.77%</td>
						</tr>
						<tr>
							<td>净利润：29.12亿</td>
							<td>同比：25.10%</td>
						</tr>
						<tr>
							<td><a href="http://data.eastmoney.com/bbsj/600276.html" target="_blank">毛利率</a>：86.54%</td>
							<td>净利率：23.70%</td>
						</tr>
						<tr>
							<td><a href="http://data.eastmoney.com/bbsj/600276.html" target="_blank">ROE<b title="加权净资产收益率" class="hxsjccsyl"></b></a>：17.23%</td>
							<td>负债率：10.36%</td>
						</tr>
						<tr>
							<td title="36.82亿">总股本：36.82亿</td>
							<td>总值：<span id="gt7_2">2173亿</span></td>
						</tr>
						<tr>
							<td title="36.62亿">流通股：36.62亿</td>
							<td>流值：<span id="gt14_2">2161亿</span></td>
						</tr>
						<tr>
							<td colspan="2">每股未分配利润：3.509元</td>
						</tr>
						<tr>
							<td colspan="2" class="pb3">上市时间：2000-10-18</td>
						</tr>
					</tbody>
					'''		
	str_gupiao_dcw_filename=datafilepath+str_gupiao_code+"_dcw"+".html"
	cur_pb='999'#市净率
	cur_mktcap='0'#总市值
	cur_nmc='0'#流通市值	
	cur_pe='999'#市盈率
	cur_roe='0'#净资产收益率
	cur_tdr='100'#ratio of liabilities to assets;　资产负债率
	cur_roa='0'#总资产收益率
	cur_jidu=4#Q4
	float_pb=999
	float_pe=999
	float_mktcap=0
	float_nmc=0
	float_roe=0	
	float_tdr=100
	float_roa=0
	
	if os.path.exists(str_gupiao_dcw_filename):
		#input_dcw_file1=codecs.open(str_gupiao_dcw_filename, 'r','gb2312')
		input_dcw_file1=codecs.open(str_gupiao_dcw_filename, 'r','GB18030')
		str_pattern_a1='<td>市净率：<span id="gt13_2">'
		str_pattern_a2='</span>'
		str_pattern_b1='<td>总值：<span id="gt7_2">'
		str_pattern_b2='亿</span></td>'
		str_pattern_c1='<td>流值：<span id="gt14_2">'
		str_pattern_c2='亿</span></td>'
		str_pattern_d1='<td>PE(动)：<span id="gt6_2">'
		str_pattern_d2='</span></td>'
		str_pattern_e1='ROE<b title="加权净资产收益率" class="hxsjccsyl"></b></a>：'
		str_pattern_e2='%</td>'
		str_pattern_f1='<td>负债率：'
		str_pattern_f2='%</td>'
		str_pattern_j1='季度">'
		str_pattern_j2='</span>'
		for line in input_dcw_file1:
			str_line=line.strip()
			#pb
			str_pattern1=str_pattern_a1
			str_pattern2=str_pattern_a2
			len_str_pattern1=len(str_pattern1)			
			seek_offset1=str_line.find(str_pattern1)
			if (not seek_offset1==-1):
				sub_str1=str_line[seek_offset1+len_str_pattern1:]
				print(sub_str1)
				seek_offset2=sub_str1.find(str_pattern2)
				sub_str2=sub_str1[0:seek_offset2]
				print(sub_str2)
				try:
					cur_pb=sub_str2
					float_pb=float(sub_str2)
				except ValueError as e:
					print('input is not a number!')
				print(cur_pb)
				#break
			#pe
			str_pattern1=str_pattern_d1
			str_pattern2=str_pattern_d2
			len_str_pattern1=len(str_pattern1)
			seek_offset1=str_line.find(str_pattern1)
			if (not seek_offset1==-1):
				sub_str1=str_line[seek_offset1+len_str_pattern1:]
				print(sub_str1)
				seek_offset2=sub_str1.find(str_pattern2)
				sub_str2=sub_str1[0:seek_offset2]
				print(sub_str2)
				try:
					cur_pe=sub_str2
					float_pe=float(sub_str2)
				except ValueError as e:
					print('input is not a number!')
				print(cur_pe)
			#第 一二三四 季度
			str_pattern1=str_pattern_j1
			str_pattern2=str_pattern_j2
			len_str_pattern1=len(str_pattern1)
			seek_offset1=str_line.find(str_pattern1)
			if (not seek_offset1==-1):
				sub_str1=str_line[seek_offset1+len_str_pattern1:]
				print(sub_str1)
				seek_offset2=sub_str1.find(str_pattern2)
				sub_str2=sub_str1[0:seek_offset2]
				print(sub_str2)
				try:
					if sub_str2=='一':#Q1
						cur_jidu=1
					if sub_str2=='二':#Q2
						cur_jidu=2
					if sub_str2=='三':#Q3
						cur_jidu=3
					if sub_str2=='四':#Q4
						cur_jidu=4										
				except ValueError as e:
					print('input is not a valid ji du!')
				print(cur_roe)			
			#roe
			str_pattern1=str_pattern_e1
			str_pattern2=str_pattern_e2
			len_str_pattern1=len(str_pattern1)
			seek_offset1=str_line.find(str_pattern1)
			if (not seek_offset1==-1):
				sub_str1=str_line[seek_offset1+len_str_pattern1:]
				print(sub_str1)
				seek_offset2=sub_str1.find(str_pattern2)
				sub_str2=sub_str1[0:seek_offset2]
				print(sub_str2)
				try:
					cur_roe=sub_str2
					float_roe=float(sub_str2)
					float_roe=float_roe/cur_jidu*4 #calculate the predict ROE of this year
				except ValueError as e:
					print('input is not a number!')
				print(cur_roe)
			#tdr
			str_pattern1=str_pattern_f1
			str_pattern2=str_pattern_f2
			len_str_pattern1=len(str_pattern1)
			seek_offset1=str_line.find(str_pattern1)
			if (not seek_offset1==-1):
				sub_str1=str_line[seek_offset1+len_str_pattern1:]
				print(sub_str1)
				seek_offset2=sub_str1.find(str_pattern2)
				sub_str2=sub_str1[0:seek_offset2]
				print(sub_str2)
				try:
					cur_tdr=sub_str2
					float_tdr=float(sub_str2)
				except ValueError as e:
					print('input is not a number!')				
				print(cur_tdr)			
			
			#mktcap
			str_pattern1=str_pattern_b1
			str_pattern2=str_pattern_b2
			len_str_pattern1=len(str_pattern1)	
			seek_offset1=str_line.find(str_pattern1)
			if (not seek_offset1==-1):
				sub_str1=str_line[seek_offset1+len_str_pattern1:]
				print(sub_str1)
				seek_offset2=sub_str1.find(str_pattern2)
				sub_str2=sub_str1[0:seek_offset2]
				print(sub_str2)
				try:
					cur_mktcap=sub_str2
					float_mktcap=float(sub_str2)
				except ValueError as e:
					print('input is not a number!')						
				print(cur_mktcap)
			#nmc
			str_pattern1=str_pattern_c1
			str_pattern2=str_pattern_c2
			len_str_pattern1=len(str_pattern1)
			seek_offset1=str_line.find(str_pattern1)
			if (not seek_offset1==-1):
				sub_str1=str_line[seek_offset1+len_str_pattern1:]
				print(sub_str1)
				seek_offset2=sub_str1.find(str_pattern2)
				sub_str2=sub_str1[0:seek_offset2]
				print(sub_str2)
				try:
					cur_nmc=sub_str2
					float_nmc=float(sub_str2)
				except ValueError as e:
					print('input is not a number!')					
				print(cur_nmc)
				float_mktcap=float_mktcap*10000
				float_nmc=float_nmc*10000
				#roa
				float_roa=(100-float_tdr)/100*float_roe
				float_roa=round(float_roa,2)
				cur_roa=str(float_roa)
				break
		input_dcw_file1.close()
		print("%s pb=%-10.2f"%(str_gupiao_code,float_pb))
		
	str_gupiao_jz_filename=datafilepath+str_gupiao_code+"_jz"+".csv"
	code=str_gupiao_code

	if 1 or not os.path.exists(str_gupiao_jz_filename):
		output_file0=codecs.open(str_gupiao_jz_filename,'w+','utf-8')
		output_file0_header=['code','cur_pb','cur_mktcap','cur_nmc','cur_pe','cur_roe','cur_tdr','cur_roa']
		print(output_file0_header)
		str_output_file0_header=",".join(output_file0_header)
		output_file0.write(str_output_file0_header)
		output_file0.write("\n")
		output_file0_line=[code,str(float_pb),str(float_mktcap),str(float_nmc),str(float_pe),str(float_roe),str(float_tdr),str(float_roa)]
		print(output_file0_line)

		output_file0_line_allstr=[]
		for x in output_file0_line:
			output_file0_line_allstr.append(str(x))
			
		str_output_file0_line=",".join(output_file0_line_allstr)
		output_file0.write(str_output_file0_line)
		output_file0.close()


def get_gupiao_list_curpb(gupiao_code_list,startk,endk):
	print('sub process pid',os.getpid())
	for k in range(startk,endk):
		str_item=gupiao_code_list[k]
		str_gupiao_code=str_item
		get_gupiao_curpb(str_gupiao_code)
		
'''
https://docs.python.org/3.5/library/multiprocessing.html
Process 类

Process 类用来描述一个进程对象。创建子进程的时候，只需要传入一个执行函数和函数的参数即可完成 Process 示例的创建。

star() 方法启动进程，
join() 方法实现进程间的同步，等待所有进程退出。
close() 用来阻止多余的进程涌入进程池 Pool 造成进程阻塞。

multiprocessing.Process(group=None, target=None, name=None, args=(), kwargs={}, *, daemon=None)

target 是函数名字，需要调用的函数
args 函数需要的参数，以 tuple 的形式传入
import multiprocessing
import os
def run_proc(name):
  print('Child process {0} {1} Running '.format(name, os.getpid()))
if __name__ == '__main__':
  print('Parent process {0} is Running'.format(os.getpid()))
  for i in range(5):
    p = multiprocessing.Process(target=run_proc, args=(str(i),))
    print('process start')
    p.start()
  p.join()
  print('Process close')
'''

		
		
item_total_num=len(output_gupiao_code_list)
print(item_total_num)
#the_cpu_num=cpu_count()*10
the_cpu_num=cpu_count()
print(the_cpu_num)

the_shang=item_total_num//the_cpu_num
the_yushu=item_total_num%the_cpu_num

my_process_list=[]

for cpu_k in range(0,the_cpu_num):
	startk=the_shang*cpu_k
	endk=the_shang*(cpu_k+1)
	print(item_total_num,cpu_k,the_shang,the_yushu,startk,endk)
	pk=Process(target=get_gupiao_list_curpb, args=(output_gupiao_code_list,startk,endk))
	my_process_list.append(pk)
	pk.start()

if 	the_yushu>0:
	startk=the_shang*the_cpu_num
	endk=item_total_num
	print(item_total_num,cpu_k,the_shang,the_yushu,startk,endk)
	pk=Process(target=get_gupiao_list_curpb, args=(output_gupiao_code_list,startk,endk))
	my_process_list.append(pk)
	pk.start()
	
for pk in my_process_list:
	pk.join()
	pass




print('item_total_num:',item_total_num)
print('the_cpu_num:',the_cpu_num)
print('main process end',os.getpid())

	

	


