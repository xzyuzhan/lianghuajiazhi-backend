#!/usr/bin/python3

from operator import itemgetter
import codecs
import urllib
import urllib.request  
import re
import os
import time
import datetime
import math
from sys import argv



#test data
str_gupiao_code="600030"
str_gupiao_code="002049"

str_gupiao_code="600362"
str_gupiao_code="002024"
str_gupiao_code="601398"
str_gupiao_code="300024"
str_gupiao_code="600489"
str_gupiao_code="000960"
str_gupiao_code="002024"
str_gupiao_code="000776"
str_gupiao_code="601211"
str_gupiao_code="600547"
str_gupiao_code="600019"
str_gupiao_code="601600"
str_gupiao_code="600219"
str_gupiao_code="601899"
str_gupiao_code="000060"
str_gupiao_code="601989"
str_gupiao_code="601989"
str_gupiao_code="002129"
str_gupiao_code="601088"
str_gupiao_code="002931"
str_gupiao_code="000591"
str_gupiao_code="600837"
str_gupiao_code="603019"


if len(argv)==1:
	str_gupiao_code="600837"
	print("notice:gu piao code is not specified,6000837 will be used as default [gu piao code]!")
else:
	str_gupiao_code=argv[1]
print("An example of a call to this script:")
print("minpb [gu piao code]   #[gu piao code] is an integer such as 600837")
the_input_str=''
while the_input_str!='0':
	the_input_str = input("请输入gu piao code：")
	if the_input_str=='q' or the_input_str=='0' or the_input_str =='quit' or the_input_str=='exit':
		print('bye!')
		break
	
	if the_input_str!='':
		str_gupiao_code=the_input_str
		

	if str_gupiao_code[0]=="6":
		shanghai_shenzhen_flag=0
		dwf_link_shanghai_shenzhen_flag="sh"
	else:
		shanghai_shenzhen_flag=1
		dwf_link_shanghai_shenzhen_flag="sz"

	os_slash=os.sep

	if os.path.exists('/storage/sdcard1'): 
		datafilepath = '/storage/sdcard1/tempdata/'
	else:
		datafilepath = os.getcwd() + os_slash +"tempdata"	+os_slash
	
	print("this path will be created,for save data",datafilepath);
	if not os.path.exists(datafilepath):
		os.makedirs(datafilepath)




	the_now=datetime.datetime.now()
	print(the_now)
	the_year=int(the_now.strftime("%Y"))

	the_month=int(the_now.strftime("%m"))

	the_day=int(the_now.strftime("%d"))


	this_year=the_year

	look_years_num=this_year-2008


	start_year=this_year-look_years_num
	end_year=this_year-1
	str_start_day="%s0101"%(start_year)
	str_end_day="%s1231"%(end_year)


	url_3="http://quote.eastmoney.com/%s%s.html"%(dwf_link_shanghai_shenzhen_flag,str_gupiao_code)

	str_gupiao_dcw_filename=datafilepath+str_gupiao_code+"_dcw"+".html"
	if os.path.exists(str_gupiao_dcw_filename):

		try:
			os.remove(str_gupiao_dcw_filename)          
		except Exception as e:                                             
			print(e)
				
	if 1 or not os.path.exists(str_gupiao_dcw_filename):
		print("access:",url_3,".............");
		try:		
			urllib.request.urlretrieve(url_3, str_gupiao_dcw_filename)#this line is useful ,but too slow.please use it when need 
		except Exception as e:                                             
			print(e)
			continue

	cur_pb="999"
	if os.path.exists(str_gupiao_dcw_filename):
		input_dcw_file1=codecs.open(str_gupiao_dcw_filename, 'r','gbk')#gb2312 -> gbk

		str_pattern1="<span id=\"gt13_2\">"
		str_pattern2="</span>"
		len_str_pattern1=len(str_pattern1)
		for line in input_dcw_file1:
			str_line=line.strip()
			seek_offset1=str_line.find(str_pattern1)
			if (not seek_offset1==-1):
				sub_str1=str_line[seek_offset1+len_str_pattern1:]
				print(sub_str1)
				seek_offset2=sub_str1.find(str_pattern2)
				sub_str2=sub_str1[0:seek_offset2]
				print(sub_str2)
				cur_pb=sub_str2
				float_cur_pb=float(sub_str2)
				break	
		input_dcw_file1.close()
		print("%s pb=%-10.2f"%(str_gupiao_code,float_cur_pb))
		
	str_gupiao_jz_filename=datafilepath+str_gupiao_code+"_jz"+".csv"
	code=str_gupiao_code

	output_file0=codecs.open(str_gupiao_jz_filename,'w+','utf-8')
	output_file0_header=["code","cur_pb"]
	print(output_file0_header)
	str_output_file0_header=",".join(output_file0_header)
	output_file0.write(str_output_file0_header)
	output_file0.write("\n")
	output_file0_line=[code,cur_pb]
	print(output_file0_line)

	output_file0_line_allstr=[]
	for x in output_file0_line:
		output_file0_line_allstr.append(str(x))
		
	str_output_file0_line=",".join(output_file0_line_allstr)
	output_file0.write(str_output_file0_line)
	output_file0.close()


	print("go to get the gu piao data of %s from %s to %s:"%(str_gupiao_code,str_start_day,str_end_day))
	url_1="http://quotes.money.163.com/service/chddata.html?code=%s%s&start=%s&end=%s"%(shanghai_shenzhen_flag,str_gupiao_code,str_start_day,str_end_day)

	url_2="http://quotes.money.163.com/service/zycwzb_%s.html?type=report"%(str_gupiao_code)

	str_gupiao_history_price_filename=datafilepath+str_gupiao_code+"_history_price"+".csv"
	if not os.path.exists(str_gupiao_history_price_filename):
		print("access:",url_1,".............");
		urllib.request.urlretrieve(url_1, str_gupiao_history_price_filename)


	str_gupiao_history_report_filename=datafilepath+str_gupiao_code+"_history_report"+".csv"
	if not os.path.exists(str_gupiao_history_report_filename):
		print("access:",url_2,".............");
		urllib.request.urlretrieve(url_2, str_gupiao_history_report_filename)



	str_gupiao_minpb2_filename=datafilepath+str_gupiao_code+"_minpb2"+".csv"

	if 1 or not os.path.exists(str_gupiao_minpb2_filename):
		


		date_history=[]
		tmv_history=[]
		bv_history=[]
		pb_history=[]
		net_profit_history=[]
		total_assets_history=[]
		avg_roa=0

		one_year_month_number=4
		look_years_num=look_years_num 
		look_report_num=4*look_years_num

		for y in range(1,look_years_num+1):
			for m in range(0,one_year_month_number):
				temp_year_str=str(this_year-y)
				temp_month_str="%02d"%(12-m*3)
				if temp_month_str=="12":
					temp_day_str="31"
				elif temp_month_str=="03":
					temp_day_str="31"
				else: ##06,09
					temp_day_str="30"		
				temp_date_str="-".join([temp_year_str,temp_month_str,temp_day_str])
				date_history.append(temp_date_str);
			





		str_today=time.strftime('%Y-%m-%d',time.localtime(time.time()))


		print("-----")

		date_row_n=0
		bv_row_n=18
		net_profit_row_n=10
		total_assets_row_n=14
		input_file2=codecs.open(str_gupiao_history_report_filename, 'r','gbk')#try gb2312 -> gbk?
		k=0

		for line in input_file2:
			if (k==date_row_n):
				temp_date_line=line.split(",")
			if (k==bv_row_n):
				temp_bv_line=line.split(",")	

			k=k+1


		k=0
		for k in range(0,look_report_num):
			tmv_history.append("-")
			bv_history.append("-")	
			pb_history.append("-")

		
		len_of_temp_date_line=len(temp_date_line)
		
		if len_of_temp_date_line<4:
			print("jump gen the minpb of ci xin gu!")#TODO:
			exit()
		
		k=0
		flag_hit=0
		for one_col in temp_date_line:
			if date_history[0]==one_col:
				flag_hit=1
				break;
			k=k+1
		
		col_offset=0
		if k==1:
			col_offset=1
		if k==2:
			col_offset=2
		if k==3:
			col_offset=3	
		if k==4:
			if flag_hit==1:
				col_offset=4	
		
		k=0
		for one_col in temp_bv_line:

			
			if k>0:


				
				if k-col_offset>=0 and k-col_offset<look_report_num:


			
					if col_offset==0:
						bv_history[k-col_offset]=one_col
				


					if col_offset==1:
						bv_history[k-col_offset]=one_col

						
						
					if col_offset==2:
						bv_history[k-col_offset]=one_col

						
					if col_offset==3:
						bv_history[k-col_offset]=one_col

						
					if col_offset==4:
						bv_history[k-col_offset]=one_col


			k=k+1
			
		input_file2.close()


		input_file1=codecs.open(str_gupiao_history_price_filename, 'r','gbk')#try gb2312 -> gbk?

		str_input_file1_header=input_file1.readline()
		strlist_input_file1_header=str_input_file1_header.split(",")


		tmv_col_n=13

		prev_dt_tmpline0=str(this_year)+"-01-01"




		k=0
		the_days_after_ipo=0
		gupiao_name=""
		gupiao_name_col_n=2
		for line in input_file1:
			the_days_after_ipo=the_days_after_ipo+1
			tmpline=line.split(",")
			for t in date_history:
				dt_tmpline0=tmpline[0]
				dt_t=t
				if (dt_t<prev_dt_tmpline0 and dt_t>=dt_tmpline0):

					tmv_history[k]=tmpline[tmv_col_n];	
					k=k+1
					if gupiao_name=="":
						gupiao_name=tmpline[gupiao_name_col_n]
					if k>=look_report_num:
						break
			if k>=look_report_num:
				break
			prev_dt_tmpline0=dt_tmpline0		

		input_file1.close()


		k=0
		for k in range(0,look_report_num):
			if tmv_history[k]=="-":
				continue
			if bv_history[k]=="-":
				continue
			f_tmv = float(tmv_history[k])
			if bv_history[k]=="--":
				continue
			f_bv = float(bv_history[k])
			if not f_bv==0.0:
				pb_history[k]=round(f_tmv/10000/f_bv,3)






	print("####output reuslt####")
	print("today:",str_today)

	print("------------------------")
	the_pb2='-'
	if 1:
		the_pb2_end_year=end_year
		the_pb2_end_month=12
		jz_data_filepath=os.getcwd()+os.sep +'getalltoday'+os.sep+'data_history'+os.sep+str(the_pb2_end_year)+'_'+str(the_pb2_end_month)+os.sep
		str_gupiao_jz_filename=jz_data_filepath+str_gupiao_code+"_jz"+".csv"	
		if  not os.path.exists(str_gupiao_jz_filename):
			print("in get jz ,error, data file for for gu piao code: %s is not exist!"%(str_gupiao_code))
		else:
			input_file=codecs.open(str_gupiao_jz_filename,'r','utf-8')
			str_header=input_file.readline()
			print(str_header)
			str_line=input_file.readline()
			print(str_line)
			str_line=str_line.strip("\r\n")	
			temp_out_line=str_line.split(",");
			print(temp_out_line)
			if temp_out_line[1]!='-':
				the_pb2=float(temp_out_line[1])
	print('the_pb2:',the_pb2)
	if pb_history[0]=='-':
		pb_history[0]=the_pb2
	
	print("[10 year pb history]")
	print("date          pb")
	print("----------    ----------")
	k=0
	for one_item in date_history:
		if pb_history[k]=="-":
			temp_str_pb="%-10s"%"-"
		else:
			temp_str_pb="%-10.03f"%(pb_history[k])
		print("%s    %s"%(one_item,temp_str_pb))
		k=k+1

	#localtime = time.asctime( time.localtime(time.time()) )
	#print('------',localtime,'------')
	print("[name]")
	print(str_gupiao_code)
	print(gupiao_name)
	print("[min pb]")
	the_minpb=0.0
	pb_history_del_null_for_min=[]
	k=0
	for temppb in pb_history:
		if not temppb=="-":
			pb_history_del_null_for_min.append(temppb)
			k=k+1
	if pb_history_del_null_for_min==[]:
		print("[waring,This gupiao has NO history pb for last year!!!!]")
		exit()
	else:
		this_func_result1="%-10.03f"%(min(pb_history_del_null_for_min))
		print(this_func_result1)
		the_minpb=float(this_func_result1)

	print("[the days after ipo]")
	this_func_result2="%-10.00f"%(the_days_after_ipo/250*350)
	print(this_func_result2)

	print("[current pb]")
	#print(cur_pb)
	the_cur_pb=float(cur_pb)
	print('%10.2f'%(the_cur_pb))
	print("[pb pao mo]")
	the_pb_paomo=round(the_cur_pb/the_minpb,2)
	print('%10.3f'%(the_pb_paomo))

	print("[risk level]")
	if the_pb_paomo<=1.1:
		print('low risk')
	elif the_pb_paomo<=1.5:
		print('middle risk')
	else:
		print('high risk')


	print('[zhi neng cang wei]')

	ratio_of_pb_to_minpb2=the_pb_paomo
	a_buy_of_per_wang_ge=3000
	zhi_neng_cang_wei=0
	max_cang_wei_per_stock=(32+0)*a_buy_of_per_wang_ge #not jin zi ta
	if ratio_of_pb_to_minpb2<=0.3:
		jin_zi_ta_l_level=32
	elif ratio_of_pb_to_minpb2>=1.5:
		jin_zi_ta_l_level=0
	else:
		jin_zi_ta_l_level=math.log((ratio_of_pb_to_minpb2/1.5),0.95)

	if jin_zi_ta_l_level<=0:
		jin_zi_ta_l_level=0

	zhi_neng_cang_wei=(0+jin_zi_ta_l_level)*a_buy_of_per_wang_ge
	gupiao_code=str_gupiao_code
	if len(gupiao_code)>=3:
		if gupiao_code=='600837' or gupiao_code=='600030' or gupiao_code=='601211' or gupiao_code=='000776' or gupiao_code=='601688' or gupiao_code=='600547' or gupiao_code=='600480':
			zhi_neng_cang_wei=zhi_neng_cang_wei*2
		if gupiao_code[0:3]=='300':
			zhi_neng_cang_wei=zhi_neng_cang_wei*0.1




	output_gupiao_code_list=[str_gupiao_code]

	str_gupiao_sn_filename=datafilepath+'the_gupiao_data'+"_sn"+".html"
	url_sn0='http://hq.sinajs.cn/list='
	url_sn1=''
	output_sn_gupiao_data_list=[]
	output_a00_beiyong_csv_data_str_line_list=[]
	k=0
	len_of_output_gupiao_code_list=len(output_gupiao_code_list)
	for a_gupiao_code in output_gupiao_code_list:
		str_gupiao_code=a_gupiao_code
		if str_gupiao_code[0]=='6':
			the_shanghai_shenzhen_flag='sh'
		else:
			the_shanghai_shenzhen_flag='sz'
		url_sn1=url_sn1+the_shanghai_shenzhen_flag+str_gupiao_code+','
		k=k+1
		if k%500==0 or k==len_of_output_gupiao_code_list:		

			urllib.request.urlretrieve(url_sn0+url_sn1, str_gupiao_sn_filename)
			input_sn_file1=codecs.open(str_gupiao_sn_filename, 'r','GB18030')
			for line in input_sn_file1:
				output_sn_gupiao_data_list.append(line)
			url_sn1=''

	trade=0

	k=0
	the_output2_lines=[]

	the_price='-'
	the_change='-'
	for str_line in output_sn_gupiao_data_list:
		k=k+1

		str_gupiao_code=str_line[13:19]

		sub_str1=str_line[21:-3]

		cols_p1 =[str(k),str_gupiao_code]
		cols_p2 = sub_str1.split(',')

		cols_all=cols_p1+cols_p2

		the_output2_lines.append(cols_all)

	trade=float(the_output2_lines[0][5])
	#the_price=trade
	the_change=(float(the_output2_lines[0][5])-float(the_output2_lines[0][4]))/float(the_output2_lines[0][4])*100
	if trade==0:
		zhi_neng_gu_fen=0
	else:
		zhi_neng_gu_fen=zhi_neng_cang_wei*1.0/trade


	zhi_neng_cang_wei=math.floor(zhi_neng_cang_wei/1000)
	zhi_neng_cang_wei=zhi_neng_cang_wei/10
	if zhi_neng_cang_wei<=0:
		zhi_neng_cang_wei=0
	str_zhi_neng_cang_wei='%3.1fw'%(round(zhi_neng_cang_wei,1))


	zhi_neng_gu_fen = int(zhi_neng_gu_fen*1.0/100)*100
	if zhi_neng_gu_fen<=0:
		zhi_neng_gu_fen=0
	str_zhi_neng_gu_fen=str(zhi_neng_gu_fen)+'股'
	str_zhi_neng_cang_wei=str_zhi_neng_cang_wei+'（'+str_zhi_neng_gu_fen+'）'
	print(str_zhi_neng_cang_wei)
	print('[stock price]')
	print('%10.3f(%10.3f%%)'%(trade,round(the_change,2)))