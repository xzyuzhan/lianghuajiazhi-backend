#!/bin/bash
###################
. /etc/profile
. ~/.profile
. ~/.bashrc

##################
cd ~/my_gitee_root/lianghuajiazhi-backend/src
cat /dev/null > nohup.out
curl 127.0.0.1:5000
if [ $? -ne 0 ]; then
    echo "connect port 5000 failed"
    nohup python3 flask_getminpb1.py &
else
    echo "connect port 5000 succeed"
fi
