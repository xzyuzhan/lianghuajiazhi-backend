
import os
from tensorflow.keras.models import Sequential
from tensorflow.keras.models import load_model
from tensorflow.keras.layers import LSTM, Dense
import tensorflow as tf


import numpy as np
import pandas as pd
import codecs
import matplotlib.pyplot as plt


import tushare as ts
import pandas as pd
import numpy as np
from sys import argv
import datetime
#from numba import jit

def predict_week_k_0day(str_gupiao_code):
	print(datetime.datetime.now())
	print(datetime.datetime.now().year)
	print(datetime.datetime.now().month)
	print(datetime.datetime.now().day)
	this_year=datetime.datetime.now().year
	last_year=this_year-1
	last_last_year=this_year-2
	datafilepath = os.getcwd() + os.sep +"predkdata"+os.sep

	if not os.path.exists(datafilepath):
		os.mkdir(datafilepath)

	the_train_start_day='%04d-%02d-%02d'%(last_last_year,1,1)
	the_train_end_day='%04d-%02d-%02d'%(datetime.datetime.now().year,datetime.datetime.now().month,datetime.datetime.now().day)
	the_predict_start_day=the_train_end_day

	print(the_train_start_day)
	print(the_train_end_day)
	print(the_predict_start_day)

	str_gupiao_price_w_qfq_filename=datafilepath+'%sw.csv'%(str_gupiao_code)

	str_gupiao_price_w_k_predict_filename=datafilepath+'predict_week_K_%s.csv'%(str_gupiao_code)
	str_train_data_filename=datafilepath+'%sw_train_data.csv'%(str_gupiao_code)
	str_predict_data_filename=datafilepath+'%sw_predict_data.csv'%(str_gupiao_code)

	#get train data
	if os.path.exists(str_gupiao_price_w_qfq_filename):
		print("file %s has exist."%(str_gupiao_price_w_qfq_filename))
		df=pd.read_csv(str_gupiao_price_w_qfq_filename)     #读入股票数据		
	else:
		print("download stock week price of this year and last year, and saving data to file %s ..."%(str_gupiao_price_w_qfq_filename))
		df=ts.get_k_data(str_gupiao_code, ktype='W', autype='qfq',start=the_train_start_day,end=the_train_end_day)
		df.to_csv(str_gupiao_price_w_qfq_filename)




	data=np.array(df[['close']])   #获取close价序列
	data_multi_features=np.array(df[['open','close','high','low']])   


	class_ids_forfileoutput = ["多", "空", "不明"]
	class_ids = ["rise","fall","unknown"]


	print('debug01')
	print(data)
	print(data.shape)
	if data.shape[0]<62:
		print("When the number of week K is less than 62,we can not predict!")
		the_output=[[str_gupiao_code,'不明',1.0]]	
		return the_output[0]

	if  data_multi_features[-1][3]<data_multi_features[-2][3]:
		the_output=[[str_gupiao_code,'空',0.71]]
		return the_output[0]
	print('debug02')
	data_fanzhuan=data[::-1]
	print(data_fanzhuan[0:30])	
	the30weekavg_thisweek=np.mean(data_fanzhuan[0:30,0:1],axis=0)
	print(the30weekavg_thisweek)
	the30weekavg_lastweek=np.mean(data_fanzhuan[1:31,0:1],axis=0)
	the30weekavg_prev15week=np.mean(data_fanzhuan[15:45,0:1],axis=0)
	the30weekavg_prev30week=np.mean(data_fanzhuan[30:60,0:1],axis=0)
	print(the30weekavg_prev30week,the30weekavg_prev15week,the30weekavg_lastweek,the30weekavg_thisweek)
	print(data[-1])
	if  data[-1]<=the30weekavg_thisweek :#or the30weekavg_thisweek<=the30weekavg_lastweek or the30weekavg_thisweek<=the30weekavg_prev15week or the30weekavg_thisweek<=the30weekavg_prev30week:
		the_output=[[str_gupiao_code,'空',0.51]]
	else:
		the_output=[[str_gupiao_code,'多',0.51]]
	return the_output[0]
#####################################################################################################################



	print(datetime.datetime.now())
	print(datetime.datetime.now().year)
	print(datetime.datetime.now().month)
	print(datetime.datetime.now().day)


	datafilepath = os.getcwd() + os.sep +"predkdata"	+os.sep

	if not os.path.exists(datafilepath):
		os.mkdir(datafilepath)

	the_train_start_day='%04d-%02d-%02d'%(2007,1,1)
	the_train_end_day='%04d-%02d-%02d'%(datetime.datetime.now().year,datetime.datetime.now().month,datetime.datetime.now().day)
	the_predict_start_day=the_train_end_day

	print(the_train_start_day)
	print(the_train_end_day)
	print(the_predict_start_day)





	str_gupiao_price_m_qfq_filename=datafilepath+'%sm.csv'%(str_gupiao_code)

	str_gupiao_price_m_k_predict_filename=datafilepath+'predict_month_K_%s.csv'%(str_gupiao_code)
	str_train_data_filename=datafilepath+'%sm_train_data.csv'%(str_gupiao_code)
	str_predict_data_filename=datafilepath+'%sm_predict_data.csv'%(str_gupiao_code)

	#get train data
	if os.path.exists(str_gupiao_price_m_qfq_filename):
		print("file %s has exist."%(str_gupiao_price_m_qfq_filename))
		df=pd.read_csv(str_gupiao_price_m_qfq_filename)     #读入股票数据		
	else:
		print("download stock month price of 10 years and saving data to file %s ..."%(str_gupiao_price_m_qfq_filename))
		df=ts.get_k_data(str_gupiao_code, ktype='M', autype='qfq',start=the_train_start_day,end=the_train_end_day)
		df.to_csv(str_gupiao_price_m_qfq_filename)


	data=np.array(df[['open','close','high','low']])   
	data1=np.array(df[['high']])#获取最高价序列
	print('data')
	#print(data)
	print(data.shape)

	print('data1')
	#print(data1)
	print(data1.shape)
	print(data1.shape[0])
	print(len(data1))
	the_labels=np.zeros((data1.shape[0],1),dtype=np.float32)
	#print(the_labels)
	for i in range(len(data1)-1):
		#print(i)
		#the_higest_price_of_a_month=np.max(day_price[day_num_a_month*i:day_num_a_month*(i+1)])
		if data1[i+1]>data1[i]*1:#rise
			the_label=0
		elif data1[i+1]==data1[i]*1:#unchange
			the_label=2
		else:#fall
			the_label=1
		the_labels[i]=the_label
	the_labels[-1]=2
	#print(the_labels)
	data_with_lable=np.concatenate((data, the_labels), axis=1)
	print('data_with_lable')
	#print(data_with_lable)
	print(data_with_lable.shape)

	#df2 = pd.DataFrame(data_with_lable,columns=['open','close','high','low','label'])

	#df2[['label']] = df2[['label']] .astype(int)
	#df2.to_csv(str_train_data_filename)










	#f=codecs.open(str_gupiao_price_m_qfq_filename, 'r','gbk')  
	#df=pd.read_csv(f)     #读入股票数据
	data=np.array(df['high'])   #获取最高价序列
	data_multi_features=np.array(df[['open','close','high','low']])   




	num_of_month_price=data.shape[0]
	#print('num_of_month_price',num_of_month_price)


	price_month=data


	#print('price_month',price_month)



	next_month_rise=[]

	#class_ids_forfileoutput = ["rise", "fall", "unchange"]
	class_ids_forfileoutput = ["多", "空", "不明"]
	class_ids = ["rise[预测未来20天比本月,有新高]","fall[预测未来20天比本月,无新高]","unchage[预测未来20天比本月,不明]"]

	for i in range(num_of_month_price-1):
		#print(i)
		#the_higest_price_of_a_month=np.max(day_price[day_num_a_month*i:day_num_a_month*(i+1)])
		if price_month[i+1]>price_month[i]:
			the_rise_label=[1,0,0]#rise
		elif price_month[i+1]==price_month[i]:
			the_rise_label=[0,0,1]#unchange
		else:
			the_rise_label=[0,1,0]#fall
		next_month_rise.append(the_rise_label)

	#print('next_month_rise',next_month_rise)






	batch_totalnum=num_of_month_price-1

	batch_size=batch_totalnum

	#print('batch_size',batch_size)
	#print('batch_totalnum',batch_totalnum)
	data=data_multi_features
	#print(data)
	#print(len(data))
	if data.shape[0]<3:
		print("When the number of month K is less than 3,we can not predict!")
		the_output=[[str_gupiao_code,'不明',1.0]]	
		return the_output[0]

	if  data_multi_features[-1][3]<data_multi_features[-2][3]:
		the_output=[[str_gupiao_code,'空',0.5]]
		return the_output[0]

	normalize_data=(data-np.mean(data,axis = 0))/np.std(data,axis = 0)  #标准化

	normalize_data=normalize_data[:,np.newaxis]       #增加维度

	x_train=np.array(normalize_data[0:-1])
	y_train=np.array(next_month_rise)
	#print('x_train=',x_train)
	#print('y_train=',y_train)




	data_dim = x_train.shape[2] # use month  price (open ,close,high,low)
	timesteps = 1 # a month and a month has no han yi.
	num_classes = 3 # rise > 10% , rise<=10% and rise>= 0%,  rise<0% 








	print(len(x_train))

	print(len(y_train))

	model=''
	this_gupiao_model_filename=datafilepath+str_gupiao_code+'_model.h5'
	if os.path.exists(this_gupiao_model_filename):
		print ('modle file %s is found,we can use it hahaha...'%(this_gupiao_model_filename))
		model = load_model(this_gupiao_model_filename)

	else:
		print ('modle file %s is NOT exist,we must train it now....'%(this_gupiao_model_filename))
		# 期望输入数据尺寸: (batch_size, timesteps, data_dim)
		# 请注意，我们必须提供完整的 batch_input_shape，因为网络是有状态的。
		# 第 k 批数据的第 i 个样本是第 k-1 批数据的第 i 个样本的后续。
		model = Sequential()
		model.add(LSTM(20, return_sequences=True, stateful=True,batch_input_shape=(batch_size, timesteps, data_dim)))
		model.add(LSTM(20, return_sequences=True, stateful=True))
		model.add(LSTM(20, stateful=True))#history month close price
		model.add(Dense(num_classes, activation='softmax'))
		model.compile(loss='categorical_crossentropy',optimizer='rmsprop',metrics=['accuracy'])
		#print(x_train)
		#print(y_train)
		#print(batch_size)
		fithistory=model.fit(x_train, y_train,batch_size=batch_size, epochs=500 ,shuffle=False,)
		#print(fithistory.history)
		predict_accuracys=fithistory.history['acc']
		#print(predict_accuracy)
		predict_accuracy=predict_accuracys[-1]
		print(predict_accuracy)
		model.save(this_gupiao_model_filename)
		del model
		model = load_model(this_gupiao_model_filename)


	predict_accuracy=1.0


	x_new=np.array(normalize_data[-1])

	x_new2_of_zero=np.zeros((batch_size,x_new.shape[0],x_new.shape[1]))
	x_new2=x_new2_of_zero
	x_new2[0]=x_new2_of_zero[0]+x_new
	print(x_new2[0])
	#print(x_new2)
	#print(x_new2.shape)



	y_new_pred = model.predict(x_new2,batch_size=batch_size)



	#print('y_new_pred',y_new_pred)


	#output predict result
	print('y_new_pred[0]',y_new_pred[0])

	class_idx=np.argmax(y_new_pred[0])
	print(class_idx)
	name = class_ids[class_idx]
	name_forfileoutput=class_ids_forfileoutput[class_idx]
	print("gupiao {} :".format(str_gupiao_code))
	chance=y_new_pred[0][class_idx]*predict_accuracy
	print('y_new_pred[0]=:%s,chance=%3.2f'%(name,chance))

	#write output to csv
	the_output=[[str_gupiao_code,name_forfileoutput,chance]]


	#if the_output[0][2]<0.5:
	#	the_output=[[str_gupiao_code,'不明',1.0]]
			

	#the_final_output=[[1,2,3]]
	print(the_output)
	df_output = pd.DataFrame(the_output,columns=['code','month_K_high_predict','chance'])
	print(df_output)
	df_output[['code']] = df_output[['code']] .astype(str)
	df_output[['month_K_high_predict']] = df_output[['month_K_high_predict']] .astype(str)
	df_output[['chance']] = df_output[['chance']] .astype(float)
	#df_output.to_csv(str_gupiao_price_m_k_predict_filename)


	tf.keras.backend.clear_session()
	#tf.reset_default_graph()

	return the_output[0]
