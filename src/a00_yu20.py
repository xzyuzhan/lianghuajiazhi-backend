from operator import itemgetter
import codecs
import urllib
import urllib.request  
import re
import os
import time
import datetime
from sys import argv
import xlrd
from threading import Thread
from multiprocessing import Process
from multiprocessing import cpu_count




#li zi 5
#http://www.csindex.com.cn/zh-CN/indices/index-detail/000985
#中证全指由全部A股股票中剔除ST、*ST股票，以及上市时间不足3个月等股票后的剩余股票构成样本股，具有较高的市场代表性。
#http://www.csindex.com.cn/uploads/file/autofile/cons/000985cons.xls
#sz50+zzzq
os_slash=os.sep
datafilepath = os.getcwd() + os_slash +"data"	+os_slash
datafilepath2 = os.getcwd() + os_slash +"getyu20today"+os_slash +"data"	+os_slash

if not os.path.exists(datafilepath):
	print("this path will be created,for save data",datafilepath);
	os.makedirs(datafilepath)



output_gupiao_code_list=[]






print(output_gupiao_code_list)
print(len(output_gupiao_code_list))


str_gupiao_code_list_filename=datafilepath2+"gupiao_code_list.txt"
print(str_gupiao_code_list_filename)
file1 = open(str_gupiao_code_list_filename)

print("文件名为: ",file1.name)
file1_line_number=0
for str_item in file1.readlines():
	file1_line_number=file1_line_number+1
	if file1_line_number==1:
		continue
	print(str_item)
	output_gupiao_code_list.append(str_item.strip())

# 关闭文件
file1.close()

print(output_gupiao_code_list)
print(len(output_gupiao_code_list))

#sn link li zi:
#http://hq.sinajs.cn/list=sh600837,sh600030,sz000776
#http://hq.sinajs.cn/list=sh600837,sh600030,sz000776,
str_gupiao_sn_filename=datafilepath+'the_gupiao_data'+"_sn"+".html"
url_sn0='http://hq.sinajs.cn/list='
url_sn1=''
output_sn_gupiao_data_list=[]
output_a00_beiyong_csv_data_str_line_list=[]
k=0
len_of_output_gupiao_code_list=len(output_gupiao_code_list)
for a_gupiao_code in output_gupiao_code_list:
	str_gupiao_code=a_gupiao_code
	if str_gupiao_code[0]=='6':
		the_shanghai_shenzhen_flag='sh'
	else:
		the_shanghai_shenzhen_flag='sz'
	url_sn1=url_sn1+the_shanghai_shenzhen_flag+str_gupiao_code+','
	k=k+1
	if k%500==0 or k==len_of_output_gupiao_code_list:		
		print(url_sn1)
		print("access:",url_sn1,".............")
		urllib.request.urlretrieve(url_sn0+url_sn1, str_gupiao_sn_filename)
		input_sn_file1=codecs.open(str_gupiao_sn_filename, 'r','GB18030')
		for line in input_sn_file1:
			output_sn_gupiao_data_list.append(line)
		url_sn1=''

print(output_sn_gupiao_data_list)

k=0
the_output2_lines=[]
colnames_all_str='NO,code,name,changepercent,trade,open,high,low,settlement,volume,turnoverratio,amount,per,pb,mktcap,nmc'
print(colnames_all_str)
for str_line in output_sn_gupiao_data_list:
	k=k+1
	'''
	var hq_str_sz300367="¶«·½ÍøÁ¦,9.750,9.460,9.810,9.930,9.600,9.800,9.810,16357384,160400660.090,85400,9.800,24300,9.790,33400,9.780,33600,9.770,32400,9.760,6200,9.810,35000,9.820,20700,9.830,14250,9.840,17600,9.850,2018-12-03,15:18:03,00"; 	
		'var hq_str_sz'
		'var hq_str_sh'
	var hq_str_sh601006="大秦铁路, 27.55, 27.25, 26.91, 27.55, 26.20, 26.91, 26.92,
	22114263, 589824680, 4695, 26.91, 57590, 26.90, 14700, 26.89, 14300,
	26.88, 15100, 26.87, 3100, 26.92, 8900, 26.93, 14230, 26.94, 25150, 26.95, 15220, 26.96, 2008-01-11, 15:05:32";
	这个字符串由许多数据拼接在一起，不同含义的数据用逗号隔开了，按照程序员的思路，顺序号从0开始。
	0：”大秦铁路”，股票名字；
	1：”27.55″，今日开盘价；
	2：”27.25″，昨日收盘价；
	3：”26.91″，当前价格；
	4：”27.55″，今日最高价；
	5：”26.20″，今日最低价；
	6：”26.91″，竞买价，即“买一”报价；
	7：”26.92″，竞卖价，即“卖一”报价；
	8：”22114263″，成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百；
	9：”589824680″，成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万；
	10：”4695″，“买一”申请4695股，即47手；
	11：”26.91″，“买一”报价；
	12：”57590″，“买二”
	13：”26.90″，“买二”
	14：”14700″，“买三”
	15：”26.89″，“买三”
	16：”14300″，“买四”
	17：”26.88″，“买四”
	18：”15100″，“买五”
	19：”26.87″，“买五”
	20：”3100″，“卖一”申报3100股，即31手；
	21：”26.92″，“卖一”报价
	(22, 23), (24, 25), (26,27), (28, 29)分别为“卖二”至“卖四的情况”
	30：”2008-01-11″，日期；
	31：”15:05:32″，时间；
	'''
	str_gupiao_code=str_line[13:19]
	#str_gupiao_name=str_line[21:25]
	sub_str1=str_line[21:-3]
	#print(str_gupiao_code,sub_str1)
	cols_p1 =[str(k),str_gupiao_code]
	cols_p2 = sub_str1.split(',')
	#print(cols_p1)
	#print(cols_p2)	
	#cols_all=cols_p1.extend(cols_p2)
	cols_all=cols_p1+cols_p2
	#print(cols_all)
	the_output2_lines.append(cols_all)
	#output_a00_beiyong_csv_data_str_line_list='%s,%s,%s'%(str(k),str_gupiao_code,sub_str1)
#print(the_output2_lines)

'''
600276_jz.csv 
code,cur_pb,cur_mktcap,cur_nmc,cur_pe,cur_roe,cur_tdr,cur_roa
600276,11.71,21720000.0,21610000.0,55.95,17.23,10.36,15.44
'''
def get_gupiao_pb_and_etc(str_gupiao_code):
	#getalltoday/data/600030_jz.csv
	str_gupiao_jz_filename=datafilepath2+str_gupiao_code+"_jz"+".csv"
	code=str_gupiao_code
	the_pb='999'
	the_mktcap='0'
	the_nmc='0'
	the_pe='999'
	the_roa='0'

	print(str_gupiao_jz_filename)
	if os.path.exists(str_gupiao_jz_filename):
		the_jz_input_file0=codecs.open(str_gupiao_jz_filename,'r+','utf-8')
		for x in the_jz_input_file0:
			str_temp_one_row=x.strip()
			temp_one_row=str_temp_one_row.split(",")
			if temp_one_row[0]==str_gupiao_code:
				the_pb=temp_one_row[1]
				the_mktcap=temp_one_row[2]
				the_nmc=temp_one_row[3]
				the_pe=temp_one_row[4]
				the_roa=temp_one_row[7]
		the_jz_input_file0.close()
	return the_pb,the_mktcap,the_nmc,the_pe,the_roa

output_file_a00=codecs.open("a00.csv",'w+','utf-8')
#header='NO,code,name,changepercent,trade,open,high,low,settlement,volume,turnoverratio,amount,per,pb,mktcap,nmc'
header='NO,code,name,changepercent,trade,open,high,low,settlement,roa,turnoverratio,amount,per,pb,mktcap,nmc'
output_file_a00.write(header+'\n')
k=0
for a_item in the_output2_lines:
	k=k+1
	if k>24:
		pass#break
	#adjust the col seq begin to NO,code,name,changepercent,trade,open,high,low,settlement,volume,turnoverratio,amount,per,pb,mktcap,nmc
	initVal = '999'
	listLen = 16
	print(a_item)
	a_item_adj = [ initVal for i in range(listLen)]
	a_item_adj[0]=a_item[0]#NO
	a_item_adj[1]=a_item[1]#code
	a_item_adj[2]=a_item[2]#name
	if float(a_item[4])>0:
		a_item_adj[3]=str(round((float(a_item[5])/float(a_item[4])-1)*100,2))#changepercent=当前价格/昨日收盘价
	else:
		print('the price of last day is 0! can NOT ji suan changepercent!')
		a_item_adj[3]='0.0'
	a_item_adj[4]=a_item[5]#trade
	a_item_adj[5]=a_item[3]#open
	a_item_adj[6]=a_item[6]#high
	a_item_adj[7]=a_item[7]#low
	a_item_adj[8]=a_item[4]#settlement
	the_pb,the_mktcap,the_nmc,the_pe,the_roa=get_gupiao_pb_and_etc(a_item[1])
	#a_item_adj[9]=a_item[10]#volume
	a_item_adj[9]=the_roa#roa
	if float(the_nmc)>0:
		a_item_adj[10]=str(round(float(a_item[11])/10000/float(the_nmc)*100,2))#turnoverratio
	else:
		print('the_nmc is 0! can NOT ji suan huan shou lv!')
		a_item_adj[10]='0.0'
	a_item_adj[11]=str(round(float(a_item[11]),2))#amount
	a_item_adj[12]=the_pe#per
	a_item_adj[13]=the_pb#pb
	a_item_adj[14]=the_mktcap#mktcap
	a_item_adj[15]=the_nmc#nmc
	#adjust the col seq end
	a_item_adj_str=",".join(a_item_adj)
	print(a_item_adj_str)
	output_file_a00.write(a_item_adj_str+'\n')
output_file_a00.close()




	
	
	

	


