from flask import Flask
from flask import request
import minpb1_lib

app = Flask(__name__)
# 跨域支持
def after_request(resp):
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

app.after_request(after_request)

@app.route('/yu', methods=['GET', 'POST'])
def home():
    return '<h1>hello world,flask,yu.</h1>'


@app.route('/')
def index():
    return 'Index Page'

@app.route('/hello')
def hello():
    return 'Hello World'

@app.route('/gupiaoapp1/')
def gupiaoapp1_default():
    gupiaocode_str = '600837'
    #result_str = 'gu piao code %s' % gupiaocode
    result_str = minpb1_lib.get_minpb1(gupiaocode_str)
    return result_str

@app.route('/gupiaoapp1/<gupiaocode>')
def gupiaoapp1_gupiaocode(gupiaocode):
    #
    gupiaocode_str=str(gupiaocode)
    result_str = minpb1_lib.get_minpb1(gupiaocode_str)
    return result_str


if __name__ == '__main__':
	#app.run(debug=True,host='192.168.0.0')
	app.run(host='0.0.0.0')
